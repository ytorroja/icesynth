{
  "version": "1.2",
  "package": {
    "name": "DAC SD",
    "version": "1",
    "description": "DAC de 1 bit Sigma-Delta",
    "author": "Yago Torroja",
    "image": ""
  },
  "design": {
    "board": "alhambra-ii",
    "graph": {
      "blocks": [
        {
          "id": "4e96581f-dd60-43cf-9d48-d54baed25342",
          "type": "basic.input",
          "data": {
            "name": "",
            "pins": [
              {
                "index": "0",
                "name": "",
                "value": ""
              }
            ],
            "virtual": true,
            "clock": true
          },
          "position": {
            "x": -128,
            "y": 72
          }
        },
        {
          "id": "39638114-3d73-45fd-a5e0-da0f69729181",
          "type": "basic.input",
          "data": {
            "name": "in",
            "range": "[7:0]",
            "pins": [
              {
                "index": "7",
                "name": "",
                "value": ""
              },
              {
                "index": "6",
                "name": "",
                "value": ""
              },
              {
                "index": "5",
                "name": "",
                "value": ""
              },
              {
                "index": "4",
                "name": "",
                "value": ""
              },
              {
                "index": "3",
                "name": "",
                "value": ""
              },
              {
                "index": "2",
                "name": "",
                "value": ""
              },
              {
                "index": "1",
                "name": "",
                "value": ""
              },
              {
                "index": "0",
                "name": "",
                "value": ""
              }
            ],
            "virtual": true,
            "clock": false
          },
          "position": {
            "x": -128,
            "y": 192
          }
        },
        {
          "id": "69c4aebe-b5df-4d96-b48f-ce5732e0c48e",
          "type": "basic.output",
          "data": {
            "name": "out",
            "pins": [
              {
                "index": "0",
                "name": "",
                "value": ""
              }
            ],
            "virtual": true
          },
          "position": {
            "x": 952,
            "y": 192
          }
        },
        {
          "id": "fd4dee99-964f-40ea-be44-605b57ea75dd",
          "type": "basic.input",
          "data": {
            "name": "en",
            "pins": [
              {
                "index": "0",
                "name": "",
                "value": ""
              }
            ],
            "virtual": true,
            "clock": false
          },
          "position": {
            "x": -128,
            "y": 304
          }
        },
        {
          "id": "c6356af9-dbd8-4b38-9aa4-43b4368b6050",
          "type": "basic.code",
          "data": {
            "code": "    localparam N = 8;\r\n    \r\n    reg [N-1+2:0] sigma;\r\n\r\n    wire [N-1+2:0] in_s;\r\n    wire [N-1+2:0] out_dac;\r\n    wire out_s;\r\n\r\n    // Generate output signal\r\n    assign out = out_s;\r\n\r\n    // input_s is in[] converted to unsigned+offset and extended with 2 bits\r\n    assign in_s    = {2'b00 , ~in[N-1], in[N-2:0]};\r\n    assign out_s   = sigma[N-1+2];\r\n    assign out_dac = {out_s, out_s, {N{1'b0}}};\r\n\r\n    always @(posedge clk)\r\n        if (en)\r\n            sigma <= sigma + in_s + out_dac;\r\n",
            "params": [],
            "ports": {
              "in": [
                {
                  "name": "clk"
                },
                {
                  "name": "in",
                  "range": "[7:0]",
                  "size": 8
                },
                {
                  "name": "en"
                }
              ],
              "out": [
                {
                  "name": "out"
                }
              ]
            }
          },
          "position": {
            "x": 136,
            "y": 48
          },
          "size": {
            "width": 712,
            "height": 344
          }
        }
      ],
      "wires": [
        {
          "source": {
            "block": "4e96581f-dd60-43cf-9d48-d54baed25342",
            "port": "out"
          },
          "target": {
            "block": "c6356af9-dbd8-4b38-9aa4-43b4368b6050",
            "port": "clk"
          }
        },
        {
          "source": {
            "block": "39638114-3d73-45fd-a5e0-da0f69729181",
            "port": "out"
          },
          "target": {
            "block": "c6356af9-dbd8-4b38-9aa4-43b4368b6050",
            "port": "in"
          },
          "size": 8
        },
        {
          "source": {
            "block": "fd4dee99-964f-40ea-be44-605b57ea75dd",
            "port": "out"
          },
          "target": {
            "block": "c6356af9-dbd8-4b38-9aa4-43b4368b6050",
            "port": "en"
          }
        },
        {
          "source": {
            "block": "c6356af9-dbd8-4b38-9aa4-43b4368b6050",
            "port": "out"
          },
          "target": {
            "block": "69c4aebe-b5df-4d96-b48f-ce5732e0c48e",
            "port": "in"
          }
        }
      ]
    }
  },
  "dependencies": {}
}