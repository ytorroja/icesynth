{
  "version": "1.2",
  "package": {
    "name": "",
    "version": "",
    "description": "",
    "author": "",
    "image": ""
  },
  "design": {
    "board": "alhambra-ii",
    "graph": {
      "blocks": [
        {
          "id": "aa768039-fa78-4b88-b26b-183cd9c1a553",
          "type": "basic.output",
          "data": {
            "name": "",
            "pins": [
              {
                "index": "0",
                "name": "LED0",
                "value": "45"
              }
            ],
            "virtual": false
          },
          "position": {
            "x": -968,
            "y": -648
          }
        },
        {
          "id": "62c3a4a2-3136-47c3-9f72-55221d0f1739",
          "type": "basic.input",
          "data": {
            "name": "RX",
            "pins": [
              {
                "index": "0",
                "name": "RX",
                "value": "62"
              }
            ],
            "virtual": false,
            "clock": false
          },
          "position": {
            "x": -2416,
            "y": -352
          }
        },
        {
          "id": "2dd424a5-e616-4505-8a89-4982cdd7539d",
          "type": "basic.output",
          "data": {
            "name": "LED",
            "range": "[7:0]",
            "pins": [
              {
                "index": "7",
                "name": "LED7",
                "value": "37"
              },
              {
                "index": "6",
                "name": "LED6",
                "value": "38"
              },
              {
                "index": "5",
                "name": "LED5",
                "value": "39"
              },
              {
                "index": "4",
                "name": "LED4",
                "value": "41"
              },
              {
                "index": "3",
                "name": "LED3",
                "value": "42"
              },
              {
                "index": "2",
                "name": "LED2",
                "value": "43"
              },
              {
                "index": "1",
                "name": "LED1",
                "value": "44"
              },
              {
                "index": "0",
                "name": "D3",
                "value": "3"
              }
            ],
            "virtual": false
          },
          "position": {
            "x": -968,
            "y": -280
          }
        },
        {
          "id": "2847c696-75d2-4b8f-b012-6634582dec16",
          "type": "basic.input",
          "data": {
            "name": "",
            "pins": [
              {
                "index": "0",
                "name": "SW2",
                "value": "33"
              }
            ],
            "virtual": false,
            "clock": false
          },
          "position": {
            "x": -1352,
            "y": 152
          }
        },
        {
          "id": "e184ed46-94cb-4452-bfe2-eb6293c24c10",
          "type": "basic.input",
          "data": {
            "name": "",
            "pins": [
              {
                "index": "0",
                "name": "SW1",
                "value": "34"
              }
            ],
            "virtual": false,
            "clock": false
          },
          "position": {
            "x": -1632,
            "y": 160
          }
        },
        {
          "id": "a962971e-b390-4800-a859-b6359cda51aa",
          "type": "basic.constant",
          "data": {
            "name": "HZ",
            "value": "48000",
            "local": false
          },
          "position": {
            "x": -2296,
            "y": 80
          }
        },
        {
          "id": "87a7dc12-17ca-45b2-9564-6f40f4711f6b",
          "type": "basic.constant",
          "data": {
            "name": "HZ",
            "value": "750000",
            "local": false
          },
          "position": {
            "x": -1984,
            "y": -720
          }
        },
        {
          "id": "c57c3752-bf0e-45cc-bd64-4f9c68637931",
          "type": "basic.info",
          "data": {
            "info": "Frecuencia de \nmuestreo",
            "readonly": true
          },
          "position": {
            "x": -2296,
            "y": 40
          },
          "size": {
            "width": 184,
            "height": 56
          }
        },
        {
          "id": "98ed3119-8490-4512-a785-f9db4f8befe0",
          "type": "basic.info",
          "data": {
            "info": "Dato a mostrar",
            "readonly": true
          },
          "position": {
            "x": -1632,
            "y": 144
          },
          "size": {
            "width": 112,
            "height": 40
          }
        },
        {
          "id": "632d6aa3-1dcf-44ad-90cd-e71dff036fd6",
          "type": "ebfed3354d2f5627e64d28b4775730fcca4711fe",
          "position": {
            "x": -2296,
            "y": 184
          },
          "size": {
            "width": 96,
            "height": 64
          }
        },
        {
          "id": "916ecb0b-e47d-4119-88ad-ebb3c41cb4bf",
          "type": "359a555a147b0afd9d84e4a720ec84b7cdfbc034",
          "position": {
            "x": -1384,
            "y": -216
          },
          "size": {
            "width": 96,
            "height": 96
          }
        },
        {
          "id": "8120c7a0-a9d3-4878-959e-80cc8c7a4415",
          "type": "9d7f8fae631414bde9d1d0339e959dee072214f0",
          "position": {
            "x": -1560,
            "y": -440
          },
          "size": {
            "width": 96,
            "height": 96
          }
        },
        {
          "id": "43777634-6c55-4729-bfa1-993881157339",
          "type": "9d7f8fae631414bde9d1d0339e959dee072214f0",
          "position": {
            "x": -1568,
            "y": -216
          },
          "size": {
            "width": 96,
            "height": 96
          }
        },
        {
          "id": "c6a3f725-9970-4eb2-bad4-5c2b64a2f0ec",
          "type": "284e88761cb52c0315f70505d23ecd217ce62956",
          "position": {
            "x": -2224,
            "y": -440
          },
          "size": {
            "width": 96,
            "height": 160
          }
        },
        {
          "id": "d5789a3e-52ad-4594-987d-3e89dfb18cb4",
          "type": "359a555a147b0afd9d84e4a720ec84b7cdfbc034",
          "position": {
            "x": -1184,
            "y": -184
          },
          "size": {
            "width": 96,
            "height": 96
          }
        },
        {
          "id": "f64abf5a-1270-4b03-bc26-ed84d4cea3eb",
          "type": "basic.info",
          "data": {
            "info": "Dato a mostrar",
            "readonly": true
          },
          "position": {
            "x": -1352,
            "y": 136
          },
          "size": {
            "width": 112,
            "height": 40
          }
        },
        {
          "id": "f26152ea-0f48-4af5-b62b-9e790e850bf0",
          "type": "81613874c6152f06c06ed7014bf4235900cfcc30",
          "position": {
            "x": -1744,
            "y": -320
          },
          "size": {
            "width": 96,
            "height": 64
          }
        },
        {
          "id": "9751fb7a-62c4-4cb1-ae62-4a98e145e674",
          "type": "9d655dfcfafcdd1d9b5284b1b35e355e6a47602c",
          "position": {
            "x": -1792,
            "y": -72
          },
          "size": {
            "width": 96,
            "height": 160
          }
        },
        {
          "id": "7d79c202-0c39-4bf3-8041-c76cefa767b5",
          "type": "5cfe2b453dc3bb6f0c6cd36ec4a333e08cb8902a",
          "position": {
            "x": -1544,
            "y": -664
          },
          "size": {
            "width": 96,
            "height": 96
          }
        },
        {
          "id": "84c08906-9081-47c8-8d27-829cdaae1482",
          "type": "basic.info",
          "data": {
            "info": "Frecuencia de \nmuestreo del DAC",
            "readonly": true
          },
          "position": {
            "x": -1984,
            "y": -784
          },
          "size": {
            "width": 120,
            "height": 56
          }
        },
        {
          "id": "23a30498-4661-4bf2-a322-ae421f150619",
          "type": "ebfed3354d2f5627e64d28b4775730fcca4711fe",
          "position": {
            "x": -1984,
            "y": -616
          },
          "size": {
            "width": 96,
            "height": 64
          }
        }
      ],
      "wires": [
        {
          "source": {
            "block": "a962971e-b390-4800-a859-b6359cda51aa",
            "port": "constant-out"
          },
          "target": {
            "block": "632d6aa3-1dcf-44ad-90cd-e71dff036fd6",
            "port": "136e8d6d-892a-4f14-8d6d-0c5bc6c3e844"
          },
          "vertices": []
        },
        {
          "source": {
            "block": "e184ed46-94cb-4452-bfe2-eb6293c24c10",
            "port": "out"
          },
          "target": {
            "block": "916ecb0b-e47d-4119-88ad-ebb3c41cb4bf",
            "port": "1e637a79-4a6d-495c-bcac-9664bdbe4b94"
          },
          "vertices": [
            {
              "x": -1432,
              "y": 88
            }
          ]
        },
        {
          "source": {
            "block": "8120c7a0-a9d3-4878-959e-80cc8c7a4415",
            "port": "226cfa16-cde9-4666-9e5c-19bf28b6a763"
          },
          "target": {
            "block": "916ecb0b-e47d-4119-88ad-ebb3c41cb4bf",
            "port": "9f5a1c91-1b93-4609-877c-816ed8fd0871"
          },
          "size": 8
        },
        {
          "source": {
            "block": "43777634-6c55-4729-bfa1-993881157339",
            "port": "226cfa16-cde9-4666-9e5c-19bf28b6a763"
          },
          "target": {
            "block": "916ecb0b-e47d-4119-88ad-ebb3c41cb4bf",
            "port": "325fbba1-e929-4921-a644-95f918e6e4ee"
          },
          "size": 8
        },
        {
          "source": {
            "block": "62c3a4a2-3136-47c3-9f72-55221d0f1739",
            "port": "out"
          },
          "target": {
            "block": "c6a3f725-9970-4eb2-bad4-5c2b64a2f0ec",
            "port": "b2b2d3d9-67f5-45dc-b888-2900e085d616"
          }
        },
        {
          "source": {
            "block": "d5789a3e-52ad-4594-987d-3e89dfb18cb4",
            "port": "bf25756a-65a4-4b09-915d-494010d6f4ff"
          },
          "target": {
            "block": "2dd424a5-e616-4505-8a89-4982cdd7539d",
            "port": "in"
          },
          "size": 8
        },
        {
          "source": {
            "block": "c6a3f725-9970-4eb2-bad4-5c2b64a2f0ec",
            "port": "fdee20c0-cffb-4315-b3d1-fac9161b494c"
          },
          "target": {
            "block": "8120c7a0-a9d3-4878-959e-80cc8c7a4415",
            "port": "208a3ad9-22a5-425a-922f-94dfcf575052"
          },
          "vertices": [],
          "size": 8
        },
        {
          "source": {
            "block": "c6a3f725-9970-4eb2-bad4-5c2b64a2f0ec",
            "port": "a61f1d2b-973d-4e83-81a9-a0a248b727f8"
          },
          "target": {
            "block": "43777634-6c55-4729-bfa1-993881157339",
            "port": "208a3ad9-22a5-425a-922f-94dfcf575052"
          },
          "vertices": [
            {
              "x": -1992,
              "y": -264
            }
          ],
          "size": 8
        },
        {
          "source": {
            "block": "2847c696-75d2-4b8f-b012-6634582dec16",
            "port": "out"
          },
          "target": {
            "block": "d5789a3e-52ad-4594-987d-3e89dfb18cb4",
            "port": "1e637a79-4a6d-495c-bcac-9664bdbe4b94"
          }
        },
        {
          "source": {
            "block": "c6a3f725-9970-4eb2-bad4-5c2b64a2f0ec",
            "port": "c0a58d11-6b11-4b5d-8e67-207db9ab9a81"
          },
          "target": {
            "block": "43777634-6c55-4729-bfa1-993881157339",
            "port": "065ea371-8398-43b3-8341-287c234a3acb"
          },
          "vertices": [
            {
              "x": -2080,
              "y": -216
            }
          ]
        },
        {
          "source": {
            "block": "f26152ea-0f48-4af5-b62b-9e790e850bf0",
            "port": "664caf9e-5f40-4df4-800a-b626af702e62"
          },
          "target": {
            "block": "8120c7a0-a9d3-4878-959e-80cc8c7a4415",
            "port": "065ea371-8398-43b3-8341-287c234a3acb"
          }
        },
        {
          "source": {
            "block": "c6a3f725-9970-4eb2-bad4-5c2b64a2f0ec",
            "port": "c93e487b-8746-43fb-8724-a0ef7d9e3636"
          },
          "target": {
            "block": "f26152ea-0f48-4af5-b62b-9e790e850bf0",
            "port": "18c2ebc7-5152-439c-9b3f-851c59bac834"
          }
        },
        {
          "source": {
            "block": "c6a3f725-9970-4eb2-bad4-5c2b64a2f0ec",
            "port": "c0a58d11-6b11-4b5d-8e67-207db9ab9a81"
          },
          "target": {
            "block": "f26152ea-0f48-4af5-b62b-9e790e850bf0",
            "port": "97b51945-d716-4b6c-9db9-970d08541249"
          },
          "vertices": [
            {
              "x": -2080,
              "y": -272
            }
          ]
        },
        {
          "source": {
            "block": "632d6aa3-1dcf-44ad-90cd-e71dff036fd6",
            "port": "c138a610-b61f-4e7c-bb8a-c4f3b0b9f95c"
          },
          "target": {
            "block": "9751fb7a-62c4-4cb1-ae62-4a98e145e674",
            "port": "f76081f1-b9fb-4a18-b664-1e9748375486"
          }
        },
        {
          "source": {
            "block": "c6a3f725-9970-4eb2-bad4-5c2b64a2f0ec",
            "port": "fdee20c0-cffb-4315-b3d1-fac9161b494c"
          },
          "target": {
            "block": "9751fb7a-62c4-4cb1-ae62-4a98e145e674",
            "port": "e864596d-e7af-40f2-bbc5-5513482463b8"
          },
          "vertices": [
            {
              "x": -1856,
              "y": -88
            }
          ],
          "size": 8
        },
        {
          "source": {
            "block": "c6a3f725-9970-4eb2-bad4-5c2b64a2f0ec",
            "port": "c0a58d11-6b11-4b5d-8e67-207db9ab9a81"
          },
          "target": {
            "block": "9751fb7a-62c4-4cb1-ae62-4a98e145e674",
            "port": "b1329812-529b-4fa5-b4c8-dfa1ac73dda2"
          },
          "vertices": [
            {
              "x": -2080,
              "y": -56
            }
          ]
        },
        {
          "source": {
            "block": "916ecb0b-e47d-4119-88ad-ebb3c41cb4bf",
            "port": "bf25756a-65a4-4b09-915d-494010d6f4ff"
          },
          "target": {
            "block": "d5789a3e-52ad-4594-987d-3e89dfb18cb4",
            "port": "9f5a1c91-1b93-4609-877c-816ed8fd0871"
          },
          "size": 8
        },
        {
          "source": {
            "block": "87a7dc12-17ca-45b2-9564-6f40f4711f6b",
            "port": "constant-out"
          },
          "target": {
            "block": "23a30498-4661-4bf2-a322-ae421f150619",
            "port": "136e8d6d-892a-4f14-8d6d-0c5bc6c3e844"
          },
          "vertices": []
        },
        {
          "source": {
            "block": "7d79c202-0c39-4bf3-8041-c76cefa767b5",
            "port": "69c4aebe-b5df-4d96-b48f-ce5732e0c48e"
          },
          "target": {
            "block": "aa768039-fa78-4b88-b26b-183cd9c1a553",
            "port": "in"
          }
        },
        {
          "source": {
            "block": "d5789a3e-52ad-4594-987d-3e89dfb18cb4",
            "port": "bf25756a-65a4-4b09-915d-494010d6f4ff"
          },
          "target": {
            "block": "7d79c202-0c39-4bf3-8041-c76cefa767b5",
            "port": "39638114-3d73-45fd-a5e0-da0f69729181"
          },
          "vertices": [
            {
              "x": -1200,
              "y": -712
            },
            {
              "x": -1704,
              "y": -656
            }
          ],
          "size": 8
        },
        {
          "source": {
            "block": "9751fb7a-62c4-4cb1-ae62-4a98e145e674",
            "port": "aaf763ee-d70e-483d-8e97-23a19f75cc92"
          },
          "target": {
            "block": "d5789a3e-52ad-4594-987d-3e89dfb18cb4",
            "port": "325fbba1-e929-4921-a644-95f918e6e4ee"
          },
          "size": 8
        },
        {
          "source": {
            "block": "23a30498-4661-4bf2-a322-ae421f150619",
            "port": "c138a610-b61f-4e7c-bb8a-c4f3b0b9f95c"
          },
          "target": {
            "block": "7d79c202-0c39-4bf3-8041-c76cefa767b5",
            "port": "fd4dee99-964f-40ea-be44-605b57ea75dd"
          }
        }
      ]
    }
  },
  "dependencies": {
    "ebfed3354d2f5627e64d28b4775730fcca4711fe": {
      "package": {
        "name": "Corazon-tic-Hz",
        "version": "0.1",
        "description": "Corazón de bombeo de tics a un frecuencia parametrica en Hz",
        "author": "Juan Gonzalez-Gomez (Obijuan)",
        "image": "%3Csvg%20xmlns=%22http://www.w3.org/2000/svg%22%20width=%22197.514%22%20height=%22161.086%22%20viewBox=%220%200%2052.259014%2042.62059%22%3E%3Cpath%20d=%22M22.153%2040.47c-.727-1.25-1.853-2.474-3.987-4.332-1.156-1.006-1.86-1.565-5.863-4.658-3.138-2.425-4.704-3.77-6.519-5.601-1.813-1.831-2.88-3.29-3.794-5.191a15.321%2015.321%200%200%201-1.235-3.6c-.317-1.545-.36-2.067-.358-4.342.002-2.983.1-3.48%201.08-5.47.728-1.479%201.281-2.257%202.433-3.427C5.028%202.714%205.754%202.2%207.325%201.422%209.069.56%2010.33.333%2012.93.417c2.02.065%202.759.266%204.36%201.188%202.52%201.45%204.475%203.777%205.017%205.972.088.358.18.652.203.652.023%200%20.227-.42.453-.932.77-1.744%201.484-2.808%202.62-3.903C29.06.041%2034.542-.565%2038.974%201.912c1.81%201.012%203.283%202.485%204.425%204.424.898%201.527%201.358%203.555%201.436%206.34.113%204.035-.625%206.832-2.59%209.812-.779%201.182-1.355%201.899-2.437%203.028-1.745%201.823-3.318%203.162-7.033%205.988-2.344%201.782-3.734%202.929-5.745%204.74-1.611%201.452-4.108%203.98-4.349%204.402-.105.185-.2.336-.21.336-.012%200-.154-.23-.318-.512z%22%20fill=%22red%22/%3E%3Ctext%20y=%2266.277%22%20x=%2267.278%22%20style=%22line-height:0%25%22%20font-weight=%22400%22%20font-size=%2215.216%22%20letter-spacing=%220%22%20word-spacing=%220%22%20transform=%22matrix(.99853%200%200%201.00147%20-50.645%20-44.99)%22%20font-family=%22sans-serif%22%20fill=%22#00f%22%20stroke-width=%22.282%22%3E%3Ctspan%20style=%22-inkscape-font-specification:'sans-serif%20Bold'%22%20y=%2266.277%22%20x=%2267.278%22%20font-weight=%22700%22%20font-size=%228.695%22%3EHz%3C/tspan%3E%3C/text%3E%3Cg%20transform=%22matrix(.79321%200%200%20.79321%20-39.33%20-27.72)%22%20stroke=%22green%22%20stroke-linecap=%22round%22%3E%3Ccircle%20r=%2214.559%22%20cy=%2273.815%22%20cx=%22100.602%22%20fill=%22#ececec%22%20stroke-width=%22.608%22%20stroke-linejoin=%22round%22/%3E%3Cpath%20d=%22M106.978%2082.142h-3.353V63.316H97.54v18.678h-3.652%22%20fill=%22none%22%20stroke-width=%221.521%22/%3E%3C/g%3E%3C/svg%3E"
      },
      "design": {
        "graph": {
          "blocks": [
            {
              "id": "c138a610-b61f-4e7c-bb8a-c4f3b0b9f95c",
              "type": "basic.output",
              "data": {
                "name": ""
              },
              "position": {
                "x": 912,
                "y": 192
              }
            },
            {
              "id": "503869f1-ddfd-4d13-93ad-5f90281ba88c",
              "type": "basic.input",
              "data": {
                "name": "",
                "clock": true
              },
              "position": {
                "x": 104,
                "y": 192
              }
            },
            {
              "id": "136e8d6d-892a-4f14-8d6d-0c5bc6c3e844",
              "type": "basic.constant",
              "data": {
                "name": "",
                "value": "1",
                "local": false
              },
              "position": {
                "x": 520,
                "y": -128
              }
            },
            {
              "id": "a70d9684-3b18-4f3d-90cd-28faa893b6b2",
              "type": "basic.code",
              "data": {
                "code": "//localparam HZ;\n\n//-- Constante para dividir y obtener una frecuencia de 1Hz\nlocalparam M = 12000000/HZ;\n\n//-- Calcular el numero de bits para almacenar M\nlocalparam N = $clog2(M);\n\n//-- Cable de reset para el contador\nwire reset;\n\n//-- Registro del divisor\nreg [N-1:0] divcounter;\n\n\n//-- Contador con reset\nalways @(posedge clk)\n  if (reset)\n    divcounter <= 0;\n  else\n    divcounter <= divcounter + 1;\n\n//-- Comparador que resetea el contador cuando se alcanza el tope\nassign reset = (divcounter == M-1);\n\n//-- La salida es la señal de overflow\nassign o = reset;\n\n\n\n",
                "params": [
                  {
                    "name": "HZ"
                  }
                ],
                "ports": {
                  "in": [
                    {
                      "name": "clk"
                    }
                  ],
                  "out": [
                    {
                      "name": "o"
                    }
                  ]
                }
              },
              "position": {
                "x": 296,
                "y": -8
              },
              "size": {
                "width": 544,
                "height": 456
              }
            }
          ],
          "wires": [
            {
              "source": {
                "block": "a70d9684-3b18-4f3d-90cd-28faa893b6b2",
                "port": "o"
              },
              "target": {
                "block": "c138a610-b61f-4e7c-bb8a-c4f3b0b9f95c",
                "port": "in"
              }
            },
            {
              "source": {
                "block": "503869f1-ddfd-4d13-93ad-5f90281ba88c",
                "port": "out"
              },
              "target": {
                "block": "a70d9684-3b18-4f3d-90cd-28faa893b6b2",
                "port": "clk"
              }
            },
            {
              "source": {
                "block": "136e8d6d-892a-4f14-8d6d-0c5bc6c3e844",
                "port": "constant-out"
              },
              "target": {
                "block": "a70d9684-3b18-4f3d-90cd-28faa893b6b2",
                "port": "HZ"
              }
            }
          ]
        }
      }
    },
    "359a555a147b0afd9d84e4a720ec84b7cdfbc034": {
      "package": {
        "name": "Mux 2 a 1 de 8 bits",
        "version": "0.0.1",
        "description": "Multiplexor de 2 a 1 de 8 bits",
        "author": "Juan Gonzalez-Gomez (obijuan)",
        "image": "%3Csvg%20xmlns=%22http://www.w3.org/2000/svg%22%20width=%2280.833%22%20height=%22158.56%22%20viewBox=%220%200%2075.781585%20148.65066%22%3E%3Cpath%20d=%22M74.375%2036.836c0-12.691-6.99-24.413-18.326-30.729-11.335-6.316-25.284-6.262-36.568.141C8.198%2012.652%201.304%2024.427%201.407%2037.118v74.415c-.103%2012.69%206.79%2024.466%2018.074%2030.87%2011.284%206.403%2025.233%206.457%2036.568.14%2011.336-6.316%2018.326-18.037%2018.326-30.728z%22%20fill=%22none%22%20stroke=%22#00f%22%20stroke-width=%222.813%22%20stroke-linecap=%22round%22%20stroke-linejoin=%22round%22/%3E%3Ctext%20style=%22line-height:125%25%22%20x=%227.448%22%20y=%2291.518%22%20transform=%22matrix(1.00472%200%200%20.9953%2020.25%2033.697)%22%20font-weight=%22400%22%20font-size=%2233.509%22%20font-family=%22sans-serif%22%20letter-spacing=%220%22%20word-spacing=%220%22%3E%3Ctspan%20x=%227.448%22%20y=%2291.518%22%3E0%3C/tspan%3E%3C/text%3E%3Ctext%20style=%22line-height:125%25%22%20x=%227.359%22%20y=%2214.582%22%20transform=%22matrix(1.00472%200%200%20.9953%2020.25%2033.697)%22%20font-weight=%22400%22%20font-size=%2233.509%22%20font-family=%22sans-serif%22%20letter-spacing=%220%22%20word-spacing=%220%22%3E%3Ctspan%20x=%227.359%22%20y=%2214.582%22%3E1%3C/tspan%3E%3C/text%3E%3C/svg%3E"
      },
      "design": {
        "graph": {
          "blocks": [
            {
              "id": "9f5a1c91-1b93-4609-877c-816ed8fd0871",
              "type": "basic.input",
              "data": {
                "name": "i1",
                "range": "[7:0]",
                "clock": false,
                "size": 8
              },
              "position": {
                "x": -704,
                "y": -88
              }
            },
            {
              "id": "325fbba1-e929-4921-a644-95f918e6e4ee",
              "type": "basic.input",
              "data": {
                "name": "i0",
                "range": "[7:0]",
                "clock": false,
                "size": 8
              },
              "position": {
                "x": -704,
                "y": 0
              }
            },
            {
              "id": "bf25756a-65a4-4b09-915d-494010d6f4ff",
              "type": "basic.output",
              "data": {
                "name": "o",
                "range": "[7:0]",
                "size": 8
              },
              "position": {
                "x": -48,
                "y": 0
              }
            },
            {
              "id": "1e637a79-4a6d-495c-bcac-9664bdbe4b94",
              "type": "basic.input",
              "data": {
                "name": "sel",
                "clock": false
              },
              "position": {
                "x": -704,
                "y": 88
              }
            },
            {
              "id": "34e6d77b-15a8-4b7c-8c41-09e9b8d4d2be",
              "type": "basic.code",
              "data": {
                "code": "//-- Multiplexor de 2 a 1, \n//-- de 8 bits\n\nreg [7:0] o;\n\nalways @(*) begin\n    case(sel)\n        0: o = i0;\n        1: o = i1;\n        default: o = i0;\n    endcase\nend\n\n",
                "params": [],
                "ports": {
                  "in": [
                    {
                      "name": "i1",
                      "range": "[7:0]",
                      "size": 8
                    },
                    {
                      "name": "i0",
                      "range": "[7:0]",
                      "size": 8
                    },
                    {
                      "name": "sel"
                    }
                  ],
                  "out": [
                    {
                      "name": "o",
                      "range": "[7:0]",
                      "size": 8
                    }
                  ]
                }
              },
              "position": {
                "x": -464,
                "y": -104
              },
              "size": {
                "width": 304,
                "height": 272
              }
            }
          ],
          "wires": [
            {
              "source": {
                "block": "1e637a79-4a6d-495c-bcac-9664bdbe4b94",
                "port": "out"
              },
              "target": {
                "block": "34e6d77b-15a8-4b7c-8c41-09e9b8d4d2be",
                "port": "sel"
              }
            },
            {
              "source": {
                "block": "325fbba1-e929-4921-a644-95f918e6e4ee",
                "port": "out"
              },
              "target": {
                "block": "34e6d77b-15a8-4b7c-8c41-09e9b8d4d2be",
                "port": "i0"
              },
              "size": 8
            },
            {
              "source": {
                "block": "9f5a1c91-1b93-4609-877c-816ed8fd0871",
                "port": "out"
              },
              "target": {
                "block": "34e6d77b-15a8-4b7c-8c41-09e9b8d4d2be",
                "port": "i1"
              },
              "size": 8
            },
            {
              "source": {
                "block": "34e6d77b-15a8-4b7c-8c41-09e9b8d4d2be",
                "port": "o"
              },
              "target": {
                "block": "bf25756a-65a4-4b09-915d-494010d6f4ff",
                "port": "in"
              },
              "size": 8
            }
          ]
        }
      }
    },
    "9d7f8fae631414bde9d1d0339e959dee072214f0": {
      "package": {
        "name": "Reg_8_en",
        "version": "1",
        "description": "Registro paralelo de 8 bits con habilitación (enable)",
        "author": "Yago Torroja",
        "image": ""
      },
      "design": {
        "graph": {
          "blocks": [
            {
              "id": "096f61b6-6d5c-4907-9512-e65b25969458",
              "type": "basic.input",
              "data": {
                "name": "clk",
                "clock": true
              },
              "position": {
                "x": 80,
                "y": 176
              }
            },
            {
              "id": "208a3ad9-22a5-425a-922f-94dfcf575052",
              "type": "basic.input",
              "data": {
                "name": "d",
                "range": "[7:0]",
                "clock": false,
                "size": 8
              },
              "position": {
                "x": 80,
                "y": 248
              }
            },
            {
              "id": "226cfa16-cde9-4666-9e5c-19bf28b6a763",
              "type": "basic.output",
              "data": {
                "name": "q",
                "range": "[7:0]",
                "size": 8
              },
              "position": {
                "x": 632,
                "y": 248
              }
            },
            {
              "id": "065ea371-8398-43b3-8341-287c234a3acb",
              "type": "basic.input",
              "data": {
                "name": "enable",
                "clock": false
              },
              "position": {
                "x": 80,
                "y": 312
              }
            },
            {
              "id": "f3b434e4-0c8f-4dd7-90c7-305189a807f1",
              "type": "basic.constant",
              "data": {
                "name": "",
                "value": "0",
                "local": false
              },
              "position": {
                "x": 376,
                "y": 56
              }
            },
            {
              "id": "32106310-bfdc-41db-9a7c-2dadd5016c3f",
              "type": "basic.code",
              "data": {
                "code": "localparam N = 8;\n\nreg [N-1:0] q = INI;\n\nalways @(posedge clk)\n  if (en)\n    q <= d;",
                "params": [
                  {
                    "name": "INI"
                  }
                ],
                "ports": {
                  "in": [
                    {
                      "name": "clk"
                    },
                    {
                      "name": "d",
                      "range": "[7:0]",
                      "size": 8
                    },
                    {
                      "name": "en"
                    }
                  ],
                  "out": [
                    {
                      "name": "q",
                      "range": "[7:0]",
                      "size": 8
                    }
                  ]
                }
              },
              "position": {
                "x": 280,
                "y": 176
              },
              "size": {
                "width": 288,
                "height": 200
              }
            }
          ],
          "wires": [
            {
              "source": {
                "block": "f3b434e4-0c8f-4dd7-90c7-305189a807f1",
                "port": "constant-out"
              },
              "target": {
                "block": "32106310-bfdc-41db-9a7c-2dadd5016c3f",
                "port": "INI"
              },
              "vertices": []
            },
            {
              "source": {
                "block": "096f61b6-6d5c-4907-9512-e65b25969458",
                "port": "out"
              },
              "target": {
                "block": "32106310-bfdc-41db-9a7c-2dadd5016c3f",
                "port": "clk"
              }
            },
            {
              "source": {
                "block": "065ea371-8398-43b3-8341-287c234a3acb",
                "port": "out"
              },
              "target": {
                "block": "32106310-bfdc-41db-9a7c-2dadd5016c3f",
                "port": "en"
              }
            },
            {
              "source": {
                "block": "32106310-bfdc-41db-9a7c-2dadd5016c3f",
                "port": "q"
              },
              "target": {
                "block": "226cfa16-cde9-4666-9e5c-19bf28b6a763",
                "port": "in"
              },
              "size": 8
            },
            {
              "source": {
                "block": "208a3ad9-22a5-425a-922f-94dfcf575052",
                "port": "out"
              },
              "target": {
                "block": "32106310-bfdc-41db-9a7c-2dadd5016c3f",
                "port": "d"
              },
              "size": 8
            }
          ]
        }
      }
    },
    "284e88761cb52c0315f70505d23ecd217ce62956": {
      "package": {
        "name": "MIDI_RX",
        "version": "1",
        "description": "MIDI Receiver",
        "author": "Yasgo Torroja",
        "image": ""
      },
      "design": {
        "graph": {
          "blocks": [
            {
              "id": "c7ba17a6-c314-47ae-9f9d-55a9b59c9b3b",
              "type": "basic.input",
              "data": {
                "name": "",
                "clock": true
              },
              "position": {
                "x": -152,
                "y": -56
              }
            },
            {
              "id": "949f731c-c8bd-4031-a5e3-2892ee185231",
              "type": "basic.inputLabel",
              "data": {
                "blockColor": "fuchsia",
                "name": "clk",
                "pins": [
                  {
                    "index": "0",
                    "name": "",
                    "value": ""
                  }
                ],
                "virtual": true
              },
              "position": {
                "x": 24,
                "y": -56
              }
            },
            {
              "id": "69f2ebab-b798-48d9-b51e-5620f9d58e1a",
              "type": "basic.outputLabel",
              "data": {
                "blockColor": "fuchsia",
                "name": "clk",
                "oldBlockColor": "fuchsia"
              },
              "position": {
                "x": 824,
                "y": 40
              }
            },
            {
              "id": "8fb555e9-9375-4f53-89e1-efbcdd147784",
              "type": "basic.outputLabel",
              "data": {
                "blockColor": "fuchsia",
                "name": "clk",
                "oldBlockColor": "fuchsia"
              },
              "position": {
                "x": -32,
                "y": 120
              }
            },
            {
              "id": "b6f3994b-aa9d-41ff-9dce-39ce389c0d58",
              "type": "basic.output",
              "data": {
                "name": "COM",
                "range": "[7:0]",
                "size": 8
              },
              "position": {
                "x": 1232,
                "y": 128
              }
            },
            {
              "id": "b2b2d3d9-67f5-45dc-b888-2900e085d616",
              "type": "basic.input",
              "data": {
                "name": "RX",
                "clock": false
              },
              "position": {
                "x": -152,
                "y": 168
              }
            },
            {
              "id": "fdee20c0-cffb-4315-b3d1-fac9161b494c",
              "type": "basic.output",
              "data": {
                "name": "B1",
                "range": "[7:0]",
                "size": 8
              },
              "position": {
                "x": 1232,
                "y": 280
              }
            },
            {
              "id": "a61f1d2b-973d-4e83-81a9-a0a248b727f8",
              "type": "basic.output",
              "data": {
                "name": "B2",
                "range": "[7:0]",
                "size": 8
              },
              "position": {
                "x": 1224,
                "y": 456
              }
            },
            {
              "id": "295eb962-a63f-42b6-9b4f-a916b8f03157",
              "type": "basic.outputLabel",
              "data": {
                "blockColor": "fuchsia",
                "name": "clk",
                "oldBlockColor": "fuchsia"
              },
              "position": {
                "x": -152,
                "y": 480
              }
            },
            {
              "id": "8925a0f8-fcb4-40c4-80ef-84ec80763677",
              "type": "basic.outputLabel",
              "data": {
                "blockColor": "fuchsia",
                "name": "clk",
                "oldBlockColor": "fuchsia"
              },
              "position": {
                "x": 736,
                "y": 720
              }
            },
            {
              "id": "c93e487b-8746-43fb-8724-a0ef7d9e3636",
              "type": "basic.output",
              "data": {
                "name": "rcv2"
              },
              "position": {
                "x": 1224,
                "y": 776
              }
            },
            {
              "id": "c0a58d11-6b11-4b5d-8e67-207db9ab9a81",
              "type": "basic.output",
              "data": {
                "name": "rcv3"
              },
              "position": {
                "x": 1224,
                "y": 1088
              }
            },
            {
              "id": "74ff0852-7904-4ab6-b508-b0af0fe2fd31",
              "type": "basic.constant",
              "data": {
                "name": "baudrate",
                "value": "38400",
                "local": false
              },
              "position": {
                "x": 104,
                "y": 32
              }
            },
            {
              "id": "fe50f589-480f-4bc9-9dc1-d1f5563d198b",
              "type": "b9ce1495492d9bb52158ff7ab53777abfad9c97d",
              "position": {
                "x": 104,
                "y": 128
              },
              "size": {
                "width": 96,
                "height": 96
              }
            },
            {
              "id": "9b76c533-2905-4bb1-a6e7-948227a75ce3",
              "type": "c8094338ed4d8fb414ec1f5289d0e9331ef4271a",
              "position": {
                "x": 448,
                "y": 112
              },
              "size": {
                "width": 96,
                "height": 64
              }
            },
            {
              "id": "c348f1ce-de03-4437-99cd-40ebd87fb63b",
              "type": "9d7f8fae631414bde9d1d0339e959dee072214f0",
              "position": {
                "x": 1008,
                "y": 264
              },
              "size": {
                "width": 96,
                "height": 96
              }
            },
            {
              "id": "ead7b526-25e0-4619-845d-889ae7f9d743",
              "type": "9d7f8fae631414bde9d1d0339e959dee072214f0",
              "position": {
                "x": 1008,
                "y": 440
              },
              "size": {
                "width": 96,
                "height": 96
              }
            },
            {
              "id": "e03303fc-03d7-4faf-a498-3f1d49dea7f9",
              "type": "9d7f8fae631414bde9d1d0339e959dee072214f0",
              "position": {
                "x": 1008,
                "y": 112
              },
              "size": {
                "width": 96,
                "height": 96
              }
            },
            {
              "id": "717df349-c4ce-431d-a98d-0e418d9ff2fb",
              "type": "9690bd0ae8d2722170a71c4a94c996a56fc9ab73",
              "position": {
                "x": 744,
                "y": 536
              },
              "size": {
                "width": 96,
                "height": 96
              }
            },
            {
              "id": "c23ea645-9360-4374-9a6e-077710aefec6",
              "type": "basic.code",
              "data": {
                "code": "reg [2:0] out;\r\nreg [1:0] state = 0;\r\nreg rcv2= 0;\r\nreg rcv3= 0;\r\n\r\n\r\nparameter   waiting_command=0, \r\n            waiting_byte_one=1, \r\n            waiting_byte_two=2;\r\n\r\n// State machine\r\nalways @(posedge clk)\r\n    begin\r\n        out  <= 3'b000;\r\n        rcv2 <= 1'b0;\r\n        rcv3 <= 1'b0;\r\n        if (en)\r\n            case (state)\r\n                waiting_command:\r\n                    if (din) begin\r\n                        state <= waiting_byte_one;\r\n                        out   <= 3'b100;\r\n                    end else\r\n                        state <= waiting_command;\r\n                waiting_byte_one:\r\n                    if (din) begin\r\n                        state <= waiting_byte_one;\r\n                        out   <= 3'b100;\r\n                    end else begin\r\n                        state <= waiting_byte_two;\r\n                        out   <= 3'b010;\r\n                        rcv2  <= 1'b1;\r\n                    end\r\n                waiting_byte_two:\r\n                    if (din) begin\r\n                        state <= waiting_byte_one;\r\n                        out   <= 3'b100;\r\n                    end else begin\r\n                        state <= waiting_command;\r\n                        out   <= 3'b001;\r\n                        rcv3  <= 1'b1;\r\n                    end\r\n            endcase\r\n     end\r\n",
                "params": [],
                "ports": {
                  "in": [
                    {
                      "name": "clk"
                    },
                    {
                      "name": "din"
                    },
                    {
                      "name": "en"
                    }
                  ],
                  "out": [
                    {
                      "name": "out",
                      "range": "[2:0]",
                      "size": 3
                    },
                    {
                      "name": "rcv2"
                    },
                    {
                      "name": "rcv3"
                    }
                  ]
                }
              },
              "position": {
                "x": 120,
                "y": 352
              },
              "size": {
                "width": 504,
                "height": 936
              }
            },
            {
              "id": "447e341a-d300-494c-b2dc-8beaabee30c5",
              "type": "1c7dae7144d376f2ee4896fcc502a29110e2db37",
              "position": {
                "x": 936,
                "y": 776
              },
              "size": {
                "width": 96,
                "height": 64
              }
            },
            {
              "id": "ba3c3caf-3ae1-4f07-8cab-3b84c8a05d8b",
              "type": "1c7dae7144d376f2ee4896fcc502a29110e2db37",
              "position": {
                "x": 944,
                "y": 1088
              },
              "size": {
                "width": 96,
                "height": 64
              }
            }
          ],
          "wires": [
            {
              "source": {
                "block": "c7ba17a6-c314-47ae-9f9d-55a9b59c9b3b",
                "port": "out"
              },
              "target": {
                "block": "949f731c-c8bd-4031-a5e3-2892ee185231",
                "port": "inlabel"
              }
            },
            {
              "source": {
                "block": "8fb555e9-9375-4f53-89e1-efbcdd147784",
                "port": "outlabel"
              },
              "target": {
                "block": "fe50f589-480f-4bc9-9dc1-d1f5563d198b",
                "port": "9b46173d-7429-4d90-8701-a2eae9f88c53"
              }
            },
            {
              "source": {
                "block": "69f2ebab-b798-48d9-b51e-5620f9d58e1a",
                "port": "outlabel"
              },
              "target": {
                "block": "e03303fc-03d7-4faf-a498-3f1d49dea7f9",
                "port": "096f61b6-6d5c-4907-9512-e65b25969458"
              },
              "vertices": [
                {
                  "x": 960,
                  "y": 120
                }
              ]
            },
            {
              "source": {
                "block": "69f2ebab-b798-48d9-b51e-5620f9d58e1a",
                "port": "outlabel"
              },
              "target": {
                "block": "c348f1ce-de03-4437-99cd-40ebd87fb63b",
                "port": "096f61b6-6d5c-4907-9512-e65b25969458"
              }
            },
            {
              "source": {
                "block": "69f2ebab-b798-48d9-b51e-5620f9d58e1a",
                "port": "outlabel"
              },
              "target": {
                "block": "ead7b526-25e0-4619-845d-889ae7f9d743",
                "port": "096f61b6-6d5c-4907-9512-e65b25969458"
              },
              "vertices": []
            },
            {
              "source": {
                "block": "295eb962-a63f-42b6-9b4f-a916b8f03157",
                "port": "outlabel"
              },
              "target": {
                "block": "c23ea645-9360-4374-9a6e-077710aefec6",
                "port": "clk"
              }
            },
            {
              "source": {
                "block": "8925a0f8-fcb4-40c4-80ef-84ec80763677",
                "port": "outlabel"
              },
              "target": {
                "block": "447e341a-d300-494c-b2dc-8beaabee30c5",
                "port": "3943e194-090b-4553-9df3-88bc4b17abc2"
              }
            },
            {
              "source": {
                "block": "8925a0f8-fcb4-40c4-80ef-84ec80763677",
                "port": "outlabel"
              },
              "target": {
                "block": "ba3c3caf-3ae1-4f07-8cab-3b84c8a05d8b",
                "port": "3943e194-090b-4553-9df3-88bc4b17abc2"
              }
            },
            {
              "source": {
                "block": "fe50f589-480f-4bc9-9dc1-d1f5563d198b",
                "port": "b82422cd-6ac3-4b32-a8b8-3aff2a066775"
              },
              "target": {
                "block": "9b76c533-2905-4bb1-a6e7-948227a75ce3",
                "port": "1f5c81aa-ebb1-4cd7-87fd-b9092de9a34f"
              },
              "size": 8
            },
            {
              "source": {
                "block": "74ff0852-7904-4ab6-b508-b0af0fe2fd31",
                "port": "constant-out"
              },
              "target": {
                "block": "fe50f589-480f-4bc9-9dc1-d1f5563d198b",
                "port": "38758516-ff7d-4688-a171-e35bb9f50bd0"
              }
            },
            {
              "source": {
                "block": "9b76c533-2905-4bb1-a6e7-948227a75ce3",
                "port": "42009c44-67c6-496d-ad9f-798dc3d7acb1"
              },
              "target": {
                "block": "c23ea645-9360-4374-9a6e-077710aefec6",
                "port": "din"
              },
              "vertices": [
                {
                  "x": 56,
                  "y": 288
                }
              ]
            },
            {
              "source": {
                "block": "fe50f589-480f-4bc9-9dc1-d1f5563d198b",
                "port": "d7ebc6ce-2cde-4e33-8c9d-b439fd2cb3e0"
              },
              "target": {
                "block": "c23ea645-9360-4374-9a6e-077710aefec6",
                "port": "en"
              },
              "vertices": [
                {
                  "x": 8,
                  "y": 240
                }
              ]
            },
            {
              "source": {
                "block": "717df349-c4ce-431d-a98d-0e418d9ff2fb",
                "port": "bf5dcadd-4527-4356-abe6-325b2d789dbe"
              },
              "target": {
                "block": "ead7b526-25e0-4619-845d-889ae7f9d743",
                "port": "065ea371-8398-43b3-8341-287c234a3acb"
              },
              "vertices": [
                {
                  "x": 936,
                  "y": 520
                }
              ]
            },
            {
              "source": {
                "block": "717df349-c4ce-431d-a98d-0e418d9ff2fb",
                "port": "b0353398-ce8e-40c5-8bc6-7d4512496311"
              },
              "target": {
                "block": "c348f1ce-de03-4437-99cd-40ebd87fb63b",
                "port": "065ea371-8398-43b3-8341-287c234a3acb"
              },
              "vertices": [
                {
                  "x": 904,
                  "y": 360
                }
              ]
            },
            {
              "source": {
                "block": "717df349-c4ce-431d-a98d-0e418d9ff2fb",
                "port": "404b8a3c-1c48-483f-9349-f34a9a1d195b"
              },
              "target": {
                "block": "e03303fc-03d7-4faf-a498-3f1d49dea7f9",
                "port": "065ea371-8398-43b3-8341-287c234a3acb"
              },
              "vertices": [
                {
                  "x": 872,
                  "y": 192
                }
              ]
            },
            {
              "source": {
                "block": "fe50f589-480f-4bc9-9dc1-d1f5563d198b",
                "port": "b82422cd-6ac3-4b32-a8b8-3aff2a066775"
              },
              "target": {
                "block": "ead7b526-25e0-4619-845d-889ae7f9d743",
                "port": "208a3ad9-22a5-425a-922f-94dfcf575052"
              },
              "vertices": [
                {
                  "x": 304,
                  "y": 232
                },
                {
                  "x": 600,
                  "y": 312
                },
                {
                  "x": 760,
                  "y": 392
                }
              ],
              "size": 8
            },
            {
              "source": {
                "block": "fe50f589-480f-4bc9-9dc1-d1f5563d198b",
                "port": "b82422cd-6ac3-4b32-a8b8-3aff2a066775"
              },
              "target": {
                "block": "c348f1ce-de03-4437-99cd-40ebd87fb63b",
                "port": "208a3ad9-22a5-425a-922f-94dfcf575052"
              },
              "vertices": [
                {
                  "x": 304,
                  "y": 184
                },
                {
                  "x": 704,
                  "y": 312
                }
              ],
              "size": 8
            },
            {
              "source": {
                "block": "b2b2d3d9-67f5-45dc-b888-2900e085d616",
                "port": "out"
              },
              "target": {
                "block": "fe50f589-480f-4bc9-9dc1-d1f5563d198b",
                "port": "2f6a3bb1-2010-4f69-a978-717528dc5160"
              }
            },
            {
              "source": {
                "block": "c348f1ce-de03-4437-99cd-40ebd87fb63b",
                "port": "226cfa16-cde9-4666-9e5c-19bf28b6a763"
              },
              "target": {
                "block": "fdee20c0-cffb-4315-b3d1-fac9161b494c",
                "port": "in"
              },
              "size": 8
            },
            {
              "source": {
                "block": "ead7b526-25e0-4619-845d-889ae7f9d743",
                "port": "226cfa16-cde9-4666-9e5c-19bf28b6a763"
              },
              "target": {
                "block": "a61f1d2b-973d-4e83-81a9-a0a248b727f8",
                "port": "in"
              },
              "size": 8
            },
            {
              "source": {
                "block": "e03303fc-03d7-4faf-a498-3f1d49dea7f9",
                "port": "226cfa16-cde9-4666-9e5c-19bf28b6a763"
              },
              "target": {
                "block": "b6f3994b-aa9d-41ff-9dce-39ce389c0d58",
                "port": "in"
              },
              "size": 8
            },
            {
              "source": {
                "block": "fe50f589-480f-4bc9-9dc1-d1f5563d198b",
                "port": "b82422cd-6ac3-4b32-a8b8-3aff2a066775"
              },
              "target": {
                "block": "e03303fc-03d7-4faf-a498-3f1d49dea7f9",
                "port": "208a3ad9-22a5-425a-922f-94dfcf575052"
              },
              "vertices": [
                {
                  "x": 304,
                  "y": 312
                },
                {
                  "x": 760,
                  "y": 160
                }
              ],
              "size": 8
            },
            {
              "source": {
                "block": "c23ea645-9360-4374-9a6e-077710aefec6",
                "port": "out"
              },
              "target": {
                "block": "717df349-c4ce-431d-a98d-0e418d9ff2fb",
                "port": "fd3449b1-bc90-4312-8654-0a9d34f90f72"
              },
              "size": 3
            },
            {
              "source": {
                "block": "447e341a-d300-494c-b2dc-8beaabee30c5",
                "port": "aa84d31e-cd92-44c7-bb38-c7a4cd903a78"
              },
              "target": {
                "block": "c93e487b-8746-43fb-8724-a0ef7d9e3636",
                "port": "in"
              }
            },
            {
              "source": {
                "block": "c23ea645-9360-4374-9a6e-077710aefec6",
                "port": "rcv2"
              },
              "target": {
                "block": "447e341a-d300-494c-b2dc-8beaabee30c5",
                "port": "bf2f0c53-2d04-4cba-aa70-2df85502d24f"
              }
            },
            {
              "source": {
                "block": "c23ea645-9360-4374-9a6e-077710aefec6",
                "port": "rcv3"
              },
              "target": {
                "block": "ba3c3caf-3ae1-4f07-8cab-3b84c8a05d8b",
                "port": "bf2f0c53-2d04-4cba-aa70-2df85502d24f"
              }
            },
            {
              "source": {
                "block": "ba3c3caf-3ae1-4f07-8cab-3b84c8a05d8b",
                "port": "aa84d31e-cd92-44c7-bb38-c7a4cd903a78"
              },
              "target": {
                "block": "c0a58d11-6b11-4b5d-8e67-207db9ab9a81",
                "port": "in"
              }
            }
          ]
        }
      }
    },
    "b9ce1495492d9bb52158ff7ab53777abfad9c97d": {
      "package": {
        "name": "Serial-rx",
        "version": "0.2",
        "description": "Receptor serie asíncrono. Velocidad por defecto: 115200 baudios",
        "author": "Juan Gonzalez-Gomez (Obijuan)",
        "image": "%3Csvg%20xmlns=%22http://www.w3.org/2000/svg%22%20height=%22223.269%22%20width=%22293.137%22%20viewBox=%220%200%20274.81662%20209.31615%22%3E%3Cg%20transform=%22matrix(-1.04907%200%200%201.04907%20-113.38%20-102.544)%22%20stroke=%22#000%22%3E%3Cpath%20d=%22M-170.798%20177.526l.315%2036.011%2040.397-37.263v-33.51z%22%20fill=%22#fff%22%20stroke-width=%223.541%22%20stroke-linecap=%22round%22%20stroke-linejoin=%22round%22/%3E%3Cpath%20d=%22M-308.584%20177.892l53.235-35.7%20124.635.628-39.456%2035.7%22%20fill=%22#fff%22%20stroke-width=%223.541%22%20stroke-linecap=%22round%22%20stroke-linejoin=%22round%22/%3E%3Crect%20ry=%223.509%22%20height=%2236.325%22%20width=%22139.039%22%20y=%22178.153%22%20x=%22-308.895%22%20fill=%22#fff%22%20stroke-width=%223.541%22%20stroke-linecap=%22square%22/%3E%3Cg%20transform=%22matrix(1.88858%200%200%201.88858%20-312.436%20138.651)%22%20stroke-width=%22.938%22%20stroke-linecap=%22square%22%3E%3Ccircle%20cy=%2230.367%22%20cx=%2211.326%22%20r=%223.15%22%20fill=%22red%22/%3E%3Ccircle%20cy=%2230.367%22%20cx=%2220.611%22%20r=%223.15%22%20fill=%22#faa%22/%3E%3Ccircle%20cy=%2230.367%22%20cx=%2262.82%22%20r=%223.15%22%20fill=%22green%22/%3E%3Ccircle%20cy=%2230.367%22%20cx=%2230.554%22%20r=%223.15%22%20fill=%22red%22/%3E%3C/g%3E%3C/g%3E%3Ctext%20y=%2228.295%22%20x=%2270.801%22%20style=%22line-height:0%25%22%20font-weight=%22400%22%20font-size=%2265.172%22%20letter-spacing=%220%22%20word-spacing=%220%22%20transform=%22scale(.99853%201.00147)%22%20font-family=%22sans-serif%22%20fill=%22#00f%22%20stroke-width=%221.209%22%3E%3Ctspan%20style=%22-inkscape-font-specification:'sans-serif%20Bold'%22%20y=%2228.295%22%20x=%2270.801%22%20font-weight=%22700%22%20font-size=%2237.241%22%3ESerial%3C/tspan%3E%3C/text%3E%3Cg%20transform=%22translate(-49.71%20-39.925)%20scale(2.8106)%22%20stroke=%22green%22%20stroke-linecap=%22round%22%3E%3Ccircle%20r=%2214.559%22%20cy=%2273.815%22%20cx=%22100.602%22%20fill=%22#ececec%22%20stroke-width=%22.608%22%20stroke-linejoin=%22round%22/%3E%3Cpath%20d=%22M106.978%2082.142h-3.353V63.316H97.54v18.678h-3.652%22%20fill=%22none%22%20stroke-width=%221.521%22/%3E%3C/g%3E%3Ccircle%20cx=%22233.043%22%20cy=%2281.071%22%20r=%2240.92%22%20fill=%22#ececec%22%20stroke=%22green%22%20stroke-width=%221.71%22%20stroke-linecap=%22round%22%20stroke-linejoin=%22round%22/%3E%3Cpath%20d=%22M261.766%2092.931h-4.696V67.437h-48.103v25.295h-5.116%22%20fill=%22none%22%20stroke=%22green%22%20stroke-width=%224.275%22%20stroke-linecap=%22round%22/%3E%3Ctext%20y=%22173.032%22%20x=%2287.94%22%20style=%22line-height:0%25%22%20font-weight=%22400%22%20font-size=%2265.172%22%20letter-spacing=%220%22%20word-spacing=%220%22%20transform=%22scale(.99853%201.00147)%22%20font-family=%22sans-serif%22%20fill=%22#00f%22%20stroke-width=%221.209%22%3E%3Ctspan%20style=%22-inkscape-font-specification:'sans-serif%20Bold'%22%20y=%22173.032%22%20x=%2287.94%22%20font-weight=%22700%22%20font-size=%2237.241%22%3ERX%3C/tspan%3E%3C/text%3E%3C/svg%3E"
      },
      "design": {
        "graph": {
          "blocks": [
            {
              "id": "b82422cd-6ac3-4b32-a8b8-3aff2a066775",
              "type": "basic.output",
              "data": {
                "name": "",
                "range": "[7:0]",
                "size": 8
              },
              "position": {
                "x": 936,
                "y": 96
              }
            },
            {
              "id": "9b46173d-7429-4d90-8701-a2eae9f88c53",
              "type": "basic.input",
              "data": {
                "name": "",
                "clock": true
              },
              "position": {
                "x": -72,
                "y": 160
              }
            },
            {
              "id": "df254332-7ba1-4c4e-b14c-97b5211f8dff",
              "type": "basic.output",
              "data": {
                "name": "busy"
              },
              "position": {
                "x": 944,
                "y": 352
              }
            },
            {
              "id": "2f6a3bb1-2010-4f69-a978-717528dc5160",
              "type": "basic.input",
              "data": {
                "name": "RX",
                "clock": false
              },
              "position": {
                "x": -80,
                "y": 544
              }
            },
            {
              "id": "d7ebc6ce-2cde-4e33-8c9d-b439fd2cb3e0",
              "type": "basic.output",
              "data": {
                "name": "rcv"
              },
              "position": {
                "x": 944,
                "y": 608
              }
            },
            {
              "id": "38758516-ff7d-4688-a171-e35bb9f50bd0",
              "type": "basic.constant",
              "data": {
                "name": "",
                "value": "115200",
                "local": false
              },
              "position": {
                "x": 416,
                "y": -136
              }
            },
            {
              "id": "d84b0e8b-3264-47e9-953f-b80b712fc373",
              "type": "basic.code",
              "data": {
                "code": "//-- Constantes para obtener las velocidades estándares\n`define B115200 104 \n`define B57600  208\n`define B38400  313\n`define B19200  625\n`define B9600   1250\n`define B4800   2500\n`define B2400   5000\n`define B1200   10000\n`define B600    20000\n`define B300    40000\n\n//-- Constante para calcular los baudios\nlocalparam BAUDRATE = (BAUD==115200) ? `B115200 : //-- OK\n                      (BAUD==57600)  ? `B57600  : //-- OK\n                      (BAUD==38400)  ? `B38400  : //-- Ok\n                      (BAUD==19200)  ? `B19200  : //-- OK\n                      (BAUD==9600)   ? `B9600   : //-- OK\n                      (BAUD==4800)   ? `B4800   : //-- OK \n                      (BAUD==2400)   ? `B2400   : //-- OK\n                      (BAUD==1200)   ? `B1200   : //-- OK\n                      (BAUD==600)    ? `B600    : //-- OK\n                      (BAUD==300)    ? `B300    : //-- OK\n                      `B115200 ;  //-- Por defecto 115200 baudios\n\n\n\n\n//-- Calcular el numero de bits para almacenar el divisor\nlocalparam N = $clog2(BAUDRATE);\n\n// Sincronizacion. Evitar \n// problema de la metaestabilidad\n\nreg d1;\nreg din;\n\nalways @(posedge clk)\n d1 <= RX;\n \n//-- Din contiene el dato serie de entrada listo para usarse   \nalways @(posedge clk)\n  din <= d1;\n  \n//------ Detectar el bit de start: flanco de bajada en din\n\n//-- Registro temporal\nreg q_t0 = 0;\n\nalways @(posedge clk)\n  q_t0 <= din;\n  \n//-- El cable din_fe es un \"tic\" que aparece cuando llega el flanco de \n//-- bajada\nwire din_fe = (q_t0 & ~din);\n\n//-------- ESTADO DEL RECEPTOR\n\n//-- 0: Apagado. Esperando\n//-- 1: Encendido. Activo. Recibiendo dato\nreg state = 0;\n\nalways @(posedge clk)\n  //-- Se pasa al estado activo al detectar el flanco de bajada\n  //-- del bit de start\n  if (din_fe)\n    state <= 1'b1;\n    \n  //-- Se pasa al estado inactivo al detectar la señal rst_state    \n  else if (rst_state)\n    state<=1'b0;\n\n//------------------ GENERADOR DE BAUDIOS -----------------------------\n//-- Se activa cuando el receptor está encendido\n\n\n//-- Calcular la mitad del divisor BAUDRATE/2\nlocalparam BAUD2 = (BAUDRATE >> 1);\n\n//-- Contador del sistema, para esperar un tiempo de  \n//-- medio bit (BAUD2)\n\n//-- NOTA: podria tener N-2 bits en principio\nreg [N-1: 0] div2counter = 0;\n\n//-- Se genera primero un retraso de BAUD/2\n//-- El proceso comienza cuando el estado pasa a 1\n\nalways @(posedge clk)\n\n  //-- Contar\n  if (state) begin\n    //-- Solo cuenta hasta BAUD2, luego  \n    //-- se queda en ese valor hasta que\n    //-- ena se desactiva\n    if (div2counter < BAUD2) \n      div2counter <= div2counter + 1;\n  end else\n    div2counter <= 0;\n\n//-- Habilitar el generador de baudios principal\n//-- cuando termine este primer contador\nwire ena2 = (div2counter == BAUD2);\n\n\n//------ GENERADOR DE BAUDIOS PRINCIPAL\n\n//-- Contador para implementar el divisor\n//-- Es un contador modulo BAUDRATE\nreg [N-1:0] divcounter = 0;\n\n//-- Cable de reset para el contador\nwire reset;\n\n//-- Contador con reset\nalways @(posedge clk)\n  if (reset)\n    divcounter <= 0;\n  else\n    divcounter <= divcounter + 1;\n\n//-- Esta señal contiene el tic\nwire ov = (divcounter == BAUDRATE-1);\n\n//-- Comparador que resetea el contador cuando se alcanza el tope\nassign reset = ov | (ena2 == 0);\n\n//-- El cable con el tic para capturar cada bit lo llamamos\n//-- bit_tic, y es la señal de overflow del contador\nwire bit_tic = ov;\n\n//-------- REGISTRO DE DESPLAZAMIENTO -----------\n//-- Es el componente que pasa los bits recibidos a paralelo\n//-- La señal de desplazamiento usada es bit_tic, pero sólo cuando  \n//-- estamos en estado de encendido (state==1)\n//-- Es un registro de 9 bits: 8 bits de datos + bit de stop\n//-- El bit de start no se almacena, es el que ha servido para\n//-- arrancar el receptor\n\nreg [8:0] sr = 0;\n\nalways @(posedge clk)\n  //-- Se captura el bit cuando llega y el receptor\n  //-- esta activado\n  if (bit_tic & state)\n    sr <= {din, sr[8:1]};\n    \n//-- El dato recibido se encuentran en los 8 bits menos significativos\n//-- una vez recibidos los 9 bits\n\n//-------- CONTADOR de bits recibidos\n\n//-- Internamente usamos un bit mas\n//-- (N+1) bits\nreg [4:0] cont = 0;\n\nalways @(posedge clk)\n\n  //-- El contador se pone a 0 si hay un overflow o \n  //-- el receptor está apagado \n  if ((state==0)| ov2)\n    cont <= 0;\n  else\n    //-- Receptor activado: Si llega un bit se incrementa\n    if (bit_tic)\n      cont <= cont + 1;\n      \n//-- Comprobar overflow\nwire ov2 = (cont == 9);\n    \n//-- Esta señal de overflow indica el final de la recepción\nwire fin = ov2;\n\n//-- Se conecta al reset el biestable de estado\nwire rst_state = fin;\n\n//----- REGISTRO DE DATOS -------------------\n//-- Registro de 8 bits que almacena el dato final\n\n//-- Bus de salida con el dato recibido\nreg data = 0;\n\nalways @(posedge clk)\n\n  //-- Si se ha recibido el ultimo bit, capturamos el dato\n  //-- que se encuentra en los 8 bits de menor peso del\n  //-- registro de desplazamiento\n  if (fin)\n    data <= sr[7:0];\n\n//-- Comunicar que se ha recibido un dato\n//-- Tic de dato recibido\nreg rcv = 0;\nalways @(posedge clk)\n  rcv <= fin;\n\n//-- La señal de busy es directamente el estado del receptor\nassign busy = state;\n\n",
                "params": [
                  {
                    "name": "BAUD"
                  }
                ],
                "ports": {
                  "in": [
                    {
                      "name": "clk"
                    },
                    {
                      "name": "RX"
                    }
                  ],
                  "out": [
                    {
                      "name": "data",
                      "range": "[7:0]",
                      "size": 8
                    },
                    {
                      "name": "busy"
                    },
                    {
                      "name": "rcv"
                    }
                  ]
                }
              },
              "position": {
                "x": 152,
                "y": 0
              },
              "size": {
                "width": 616,
                "height": 768
              }
            }
          ],
          "wires": [
            {
              "source": {
                "block": "9b46173d-7429-4d90-8701-a2eae9f88c53",
                "port": "out"
              },
              "target": {
                "block": "d84b0e8b-3264-47e9-953f-b80b712fc373",
                "port": "clk"
              }
            },
            {
              "source": {
                "block": "2f6a3bb1-2010-4f69-a978-717528dc5160",
                "port": "out"
              },
              "target": {
                "block": "d84b0e8b-3264-47e9-953f-b80b712fc373",
                "port": "RX"
              }
            },
            {
              "source": {
                "block": "d84b0e8b-3264-47e9-953f-b80b712fc373",
                "port": "data"
              },
              "target": {
                "block": "b82422cd-6ac3-4b32-a8b8-3aff2a066775",
                "port": "in"
              },
              "size": 8
            },
            {
              "source": {
                "block": "d84b0e8b-3264-47e9-953f-b80b712fc373",
                "port": "rcv"
              },
              "target": {
                "block": "d7ebc6ce-2cde-4e33-8c9d-b439fd2cb3e0",
                "port": "in"
              }
            },
            {
              "source": {
                "block": "38758516-ff7d-4688-a171-e35bb9f50bd0",
                "port": "constant-out"
              },
              "target": {
                "block": "d84b0e8b-3264-47e9-953f-b80b712fc373",
                "port": "BAUD"
              }
            },
            {
              "source": {
                "block": "d84b0e8b-3264-47e9-953f-b80b712fc373",
                "port": "busy"
              },
              "target": {
                "block": "df254332-7ba1-4c4e-b14c-97b5211f8dff",
                "port": "in"
              }
            }
          ]
        }
      }
    },
    "c8094338ed4d8fb414ec1f5289d0e9331ef4271a": {
      "package": {
        "name": "Separador-bus-1-7",
        "version": "0.1",
        "description": "Separador de bus de 8-bits en 2 buses de 1 y 7 bits",
        "author": "Juan González-Gómez (Obijuan)",
        "image": "%3Csvg%20xmlns=%22http://www.w3.org/2000/svg%22%20width=%22354.768%22%20height=%22241.058%22%20viewBox=%220%200%20332.59497%20225.99201%22%3E%3Cpath%20d=%22M168.377%2077.643l61.147-60.938C240.21%206.25%20254.56.461%20269.484.5h62.611v26.186l-61.698.046c-8.012-.043-15.705%203.133-21.47%208.81L187.48%2096.857a57.292%2057.292%200%200%201-39.993%2016.139%2057.292%2057.292%200%200%201%2039.993%2016.14l61.448%2061.314c5.765%205.677%2013.458%208.853%2021.47%208.81l61.698.046v26.186h-62.612c-14.924.039-29.463-5.9-40.204-16.28l-60.902-60.863a29.857%2029.857%200%200%200-21.347-8.81L.5%20139.427V86.457h146.524a29.884%2029.884%200%200%200%2021.353-8.814z%22%20fill=%22green%22%20stroke=%22#000%22%20stroke-linecap=%22round%22%20stroke-linejoin=%22round%22/%3E%3C/svg%3E"
      },
      "design": {
        "graph": {
          "blocks": [
            {
              "id": "42009c44-67c6-496d-ad9f-798dc3d7acb1",
              "type": "basic.output",
              "data": {
                "name": ""
              },
              "position": {
                "x": 584,
                "y": 144
              }
            },
            {
              "id": "1f5c81aa-ebb1-4cd7-87fd-b9092de9a34f",
              "type": "basic.input",
              "data": {
                "name": "i",
                "range": "[7:0]",
                "clock": false,
                "size": 8
              },
              "position": {
                "x": 120,
                "y": 200
              }
            },
            {
              "id": "5c07bd65-d4e4-463c-affc-8bffa37c3b11",
              "type": "basic.output",
              "data": {
                "name": "",
                "range": "[6:0]",
                "size": 7
              },
              "position": {
                "x": 584,
                "y": 272
              }
            },
            {
              "id": "16e78204-213e-4833-9096-89d735307ec2",
              "type": "basic.code",
              "data": {
                "code": "assign o1 = i[7];\nassign o0 = i[6:0];",
                "params": [],
                "ports": {
                  "in": [
                    {
                      "name": "i",
                      "range": "[7:0]",
                      "size": 8
                    }
                  ],
                  "out": [
                    {
                      "name": "o1"
                    },
                    {
                      "name": "o0",
                      "range": "[6:0]",
                      "size": 7
                    }
                  ]
                }
              },
              "position": {
                "x": 296,
                "y": 176
              },
              "size": {
                "width": 208,
                "height": 112
              }
            }
          ],
          "wires": [
            {
              "source": {
                "block": "1f5c81aa-ebb1-4cd7-87fd-b9092de9a34f",
                "port": "out"
              },
              "target": {
                "block": "16e78204-213e-4833-9096-89d735307ec2",
                "port": "i"
              },
              "size": 8
            },
            {
              "source": {
                "block": "16e78204-213e-4833-9096-89d735307ec2",
                "port": "o1"
              },
              "target": {
                "block": "42009c44-67c6-496d-ad9f-798dc3d7acb1",
                "port": "in"
              }
            },
            {
              "source": {
                "block": "16e78204-213e-4833-9096-89d735307ec2",
                "port": "o0"
              },
              "target": {
                "block": "5c07bd65-d4e4-463c-affc-8bffa37c3b11",
                "port": "in"
              },
              "size": 7
            }
          ]
        }
      }
    },
    "9690bd0ae8d2722170a71c4a94c996a56fc9ab73": {
      "package": {
        "name": "Separador-bus",
        "version": "0.1",
        "description": "Separador de bus de 3-bits",
        "author": "Juan González-Gómez (Obijuan)",
        "image": "%3Csvg%20xmlns=%22http://www.w3.org/2000/svg%22%20width=%22354.768%22%20height=%22241.058%22%20viewBox=%220%200%20332.59497%20225.99201%22%3E%3Cpath%20d=%22M168.377%2077.643l61.147-60.938C240.21%206.25%20254.56.461%20269.484.5h62.611v26.186l-61.698.046c-8.012-.043-15.705%203.133-21.47%208.81L187.48%2096.857a57.292%2057.292%200%200%201-39.993%2016.139%2057.292%2057.292%200%200%201%2039.993%2016.14l61.448%2061.314c5.765%205.677%2013.458%208.853%2021.47%208.81l61.698.046v26.186h-62.612c-14.924.039-29.463-5.9-40.204-16.28l-60.902-60.863a29.857%2029.857%200%200%200-21.347-8.81L.5%20139.427V86.457h146.524a29.884%2029.884%200%200%200%2021.353-8.814z%22%20fill=%22green%22%20stroke=%22#000%22%20stroke-linecap=%22round%22%20stroke-linejoin=%22round%22/%3E%3C/svg%3E"
      },
      "design": {
        "graph": {
          "blocks": [
            {
              "id": "404b8a3c-1c48-483f-9349-f34a9a1d195b",
              "type": "basic.output",
              "data": {
                "name": "o2"
              },
              "position": {
                "x": 584,
                "y": 144
              }
            },
            {
              "id": "fd3449b1-bc90-4312-8654-0a9d34f90f72",
              "type": "basic.input",
              "data": {
                "name": "i",
                "range": "[2:0]",
                "clock": false,
                "size": 3
              },
              "position": {
                "x": 120,
                "y": 200
              }
            },
            {
              "id": "b0353398-ce8e-40c5-8bc6-7d4512496311",
              "type": "basic.output",
              "data": {
                "name": "o1"
              },
              "position": {
                "x": 584,
                "y": 200
              }
            },
            {
              "id": "bf5dcadd-4527-4356-abe6-325b2d789dbe",
              "type": "basic.output",
              "data": {
                "name": "o0"
              },
              "position": {
                "x": 584,
                "y": 256
              }
            },
            {
              "id": "16e78204-213e-4833-9096-89d735307ec2",
              "type": "basic.code",
              "data": {
                "code": "assign o2 = i[2];\nassign o1 = i[1];\nassign o0 = i[0];",
                "params": [],
                "ports": {
                  "in": [
                    {
                      "name": "i",
                      "range": "[2:0]",
                      "size": 3
                    }
                  ],
                  "out": [
                    {
                      "name": "o2"
                    },
                    {
                      "name": "o1"
                    },
                    {
                      "name": "o0"
                    }
                  ]
                }
              },
              "position": {
                "x": 296,
                "y": 176
              },
              "size": {
                "width": 208,
                "height": 112
              }
            }
          ],
          "wires": [
            {
              "source": {
                "block": "16e78204-213e-4833-9096-89d735307ec2",
                "port": "o2"
              },
              "target": {
                "block": "404b8a3c-1c48-483f-9349-f34a9a1d195b",
                "port": "in"
              }
            },
            {
              "source": {
                "block": "16e78204-213e-4833-9096-89d735307ec2",
                "port": "o1"
              },
              "target": {
                "block": "b0353398-ce8e-40c5-8bc6-7d4512496311",
                "port": "in"
              }
            },
            {
              "source": {
                "block": "16e78204-213e-4833-9096-89d735307ec2",
                "port": "o0"
              },
              "target": {
                "block": "bf5dcadd-4527-4356-abe6-325b2d789dbe",
                "port": "in"
              }
            },
            {
              "source": {
                "block": "fd3449b1-bc90-4312-8654-0a9d34f90f72",
                "port": "out"
              },
              "target": {
                "block": "16e78204-213e-4833-9096-89d735307ec2",
                "port": "i"
              },
              "size": 3
            }
          ]
        }
      }
    },
    "1c7dae7144d376f2ee4896fcc502a29110e2db37": {
      "package": {
        "name": "Biestable-D",
        "version": "0.1",
        "description": "Biestable de datos (Tipo D). Cuando se recibe un tic por load se captura el dato",
        "author": "Juan González-Gómez (Obijuan)",
        "image": "%3Csvg%20xmlns=%22http://www.w3.org/2000/svg%22%20width=%22156.57%22%20height=%22216.83%22%20viewBox=%220%200%2041.425941%2057.369679%22%3E%3Cg%20stroke=%22#000%22%20stroke-width=%221.442%22%20stroke-linecap=%22round%22%20stroke-linejoin=%22round%22%3E%3Cpath%20d=%22M25.682%2040.152L39.29%2056.824%2032.372%2036.29%22%20fill=%22#ccc%22%20stroke-width=%221.0924880399999999%22/%3E%3Cpath%20d=%22M18.298%2032.088L9.066%2012.475l-6.45%203.724-2.07-3.583L21.451.546%2023.52%204.13l-6.092%203.517%2012.03%2018.223s5.399-2.025%208.535.74c3.137%202.766%202.52%204.92%202.887%204.772L17.192%2045.02s-2.848-3.695-2.16-6.795c.688-3.1%203.266-6.137%203.266-6.137z%22%20fill=%22red%22%20stroke-width=%221.0924880399999999%22/%3E%3C/g%3E%3C/svg%3E"
      },
      "design": {
        "graph": {
          "blocks": [
            {
              "id": "3943e194-090b-4553-9df3-88bc4b17abc2",
              "type": "basic.input",
              "data": {
                "name": "",
                "clock": true
              },
              "position": {
                "x": 192,
                "y": 136
              }
            },
            {
              "id": "aa84d31e-cd92-44c7-bb38-c7a4cd903a78",
              "type": "basic.output",
              "data": {
                "name": ""
              },
              "position": {
                "x": 680,
                "y": 184
              }
            },
            {
              "id": "bf2f0c53-2d04-4cba-aa70-2df85502d24f",
              "type": "basic.input",
              "data": {
                "name": "",
                "clock": false
              },
              "position": {
                "x": 192,
                "y": 232
              }
            },
            {
              "id": "65194b18-5d2a-41b2-bd86-01be99978ad6",
              "type": "basic.constant",
              "data": {
                "name": "",
                "value": "0",
                "local": false
              },
              "position": {
                "x": 456,
                "y": 64
              }
            },
            {
              "id": "bdc170f0-4468-4137-bd79-4624c9cadf2b",
              "type": "basic.code",
              "data": {
                "code": "reg q = INI;\nalways @(posedge clk)\n  q <= d;",
                "params": [
                  {
                    "name": "INI"
                  }
                ],
                "ports": {
                  "in": [
                    {
                      "name": "clk"
                    },
                    {
                      "name": "d"
                    }
                  ],
                  "out": [
                    {
                      "name": "q"
                    }
                  ]
                }
              },
              "position": {
                "x": 384,
                "y": 168
              },
              "size": {
                "width": 232,
                "height": 88
              }
            }
          ],
          "wires": [
            {
              "source": {
                "block": "3943e194-090b-4553-9df3-88bc4b17abc2",
                "port": "out"
              },
              "target": {
                "block": "bdc170f0-4468-4137-bd79-4624c9cadf2b",
                "port": "clk"
              }
            },
            {
              "source": {
                "block": "bdc170f0-4468-4137-bd79-4624c9cadf2b",
                "port": "q"
              },
              "target": {
                "block": "aa84d31e-cd92-44c7-bb38-c7a4cd903a78",
                "port": "in"
              }
            },
            {
              "source": {
                "block": "65194b18-5d2a-41b2-bd86-01be99978ad6",
                "port": "constant-out"
              },
              "target": {
                "block": "bdc170f0-4468-4137-bd79-4624c9cadf2b",
                "port": "INI"
              }
            },
            {
              "source": {
                "block": "bf2f0c53-2d04-4cba-aa70-2df85502d24f",
                "port": "out"
              },
              "target": {
                "block": "bdc170f0-4468-4137-bd79-4624c9cadf2b",
                "port": "d"
              }
            }
          ]
        }
      }
    },
    "81613874c6152f06c06ed7014bf4235900cfcc30": {
      "package": {
        "name": "OR",
        "version": "1.0.1",
        "description": "Puerta OR",
        "author": "Jesús Arroyo, Juan González",
        "image": "%3Csvg%20xmlns=%22http://www.w3.org/2000/svg%22%20height=%22192.718%22%20width=%22383.697%22%20version=%221%22%3E%3Cpath%20d=%22M175.56%20188.718H84.527s30.345-42.538%2031.086-94.03c.743-51.49-31.821-90.294-31.821-90.294L176.109%204c46.445%201.948%20103.899%2053.44%20123.047%2093.678-32.601%2067.503-92.158%2089.79-123.596%2091.04z%22%20fill=%22none%22%20stroke=%22#000%22%20stroke-width=%228%22%20stroke-linecap=%22round%22%20stroke-linejoin=%22round%22/%3E%3Cpath%20d=%22M4.057%2047.292h99.605M4.883%20145.168h100.981M298.57%2098.89h81.07%22%20fill=%22none%22%20stroke=%22#000%22%20stroke-width=%228%22%20stroke-linecap=%22round%22/%3E%3Ctext%20style=%22line-height:125%25%22%20font-weight=%22400%22%20font-size=%2266.317%22%20y=%22121.28%22%20x=%22131.572%22%20font-family=%22sans-serif%22%20letter-spacing=%220%22%20word-spacing=%220%22%20fill=%22#00f%22%3E%3Ctspan%20font-weight=%22700%22%20y=%22121.28%22%20x=%22131.572%22%3EOR%3C/tspan%3E%3C/text%3E%3C/svg%3E"
      },
      "design": {
        "graph": {
          "blocks": [
            {
              "id": "18c2ebc7-5152-439c-9b3f-851c59bac834",
              "type": "basic.input",
              "data": {
                "name": ""
              },
              "position": {
                "x": 64,
                "y": 88
              }
            },
            {
              "id": "664caf9e-5f40-4df4-800a-b626af702e62",
              "type": "basic.output",
              "data": {
                "name": ""
              },
              "position": {
                "x": 784,
                "y": 152
              }
            },
            {
              "id": "97b51945-d716-4b6c-9db9-970d08541249",
              "type": "basic.input",
              "data": {
                "name": ""
              },
              "position": {
                "x": 64,
                "y": 224
              }
            },
            {
              "id": "00925b04-5004-4307-a737-fa4e97c8b6ab",
              "type": "basic.code",
              "data": {
                "code": "//-- Puerta OR\n\n//-- module and (input wire a, input wire b,\n//--             output wire c);\n\nassign c = a | b;\n\n//-- endmodule",
                "params": [],
                "ports": {
                  "in": [
                    {
                      "name": "a"
                    },
                    {
                      "name": "b"
                    }
                  ],
                  "out": [
                    {
                      "name": "c"
                    }
                  ]
                }
              },
              "position": {
                "x": 256,
                "y": 48
              },
              "size": {
                "width": 464,
                "height": 272
              }
            }
          ],
          "wires": [
            {
              "source": {
                "block": "18c2ebc7-5152-439c-9b3f-851c59bac834",
                "port": "out"
              },
              "target": {
                "block": "00925b04-5004-4307-a737-fa4e97c8b6ab",
                "port": "a"
              }
            },
            {
              "source": {
                "block": "97b51945-d716-4b6c-9db9-970d08541249",
                "port": "out"
              },
              "target": {
                "block": "00925b04-5004-4307-a737-fa4e97c8b6ab",
                "port": "b"
              }
            },
            {
              "source": {
                "block": "00925b04-5004-4307-a737-fa4e97c8b6ab",
                "port": "c"
              },
              "target": {
                "block": "664caf9e-5f40-4df4-800a-b626af702e62",
                "port": "in"
              }
            }
          ]
        }
      }
    },
    "9d655dfcfafcdd1d9b5284b1b35e355e6a47602c": {
      "package": {
        "name": "OSC",
        "version": "1",
        "description": "Oscilador con salidas diente de sierra, triangular y PWM",
        "author": "Yago Torroja",
        "image": ""
      },
      "design": {
        "graph": {
          "blocks": [
            {
              "id": "aaf763ee-d70e-483d-8e97-23a19f75cc92",
              "type": "basic.output",
              "data": {
                "name": "TRI",
                "range": "[7:0]",
                "size": 8
              },
              "position": {
                "x": 952,
                "y": -40
              }
            },
            {
              "id": "118151e1-05c3-4216-b12d-2b21049f0502",
              "type": "basic.input",
              "data": {
                "name": "",
                "clock": true
              },
              "position": {
                "x": -56,
                "y": 88
              }
            },
            {
              "id": "ab2710ae-8ee8-48a6-9ad8-21e610fcfe7c",
              "type": "basic.output",
              "data": {
                "name": "SAW",
                "range": "[7:0]",
                "size": 8
              },
              "position": {
                "x": 952,
                "y": 120
              }
            },
            {
              "id": "e864596d-e7af-40f2-bbc5-5513482463b8",
              "type": "basic.input",
              "data": {
                "name": "din",
                "range": "[7:0]",
                "clock": false,
                "size": 8
              },
              "position": {
                "x": -56,
                "y": 160
              }
            },
            {
              "id": "b1329812-529b-4fa5-b4c8-dfa1ac73dda2",
              "type": "basic.input",
              "data": {
                "name": "load",
                "clock": false
              },
              "position": {
                "x": -56,
                "y": 224
              }
            },
            {
              "id": "f76081f1-b9fb-4a18-b664-1e9748375486",
              "type": "basic.input",
              "data": {
                "name": "count",
                "clock": false
              },
              "position": {
                "x": -56,
                "y": 288
              }
            },
            {
              "id": "feecf4a9-c6c4-49c5-a065-eb4de2a73243",
              "type": "basic.output",
              "data": {
                "name": "PWM",
                "range": "[7:0]",
                "size": 8
              },
              "position": {
                "x": 944,
                "y": 352
              }
            },
            {
              "id": "3f35a684-3dbc-4164-a88a-fbd61408fd0d",
              "type": "basic.input",
              "data": {
                "name": "LVL",
                "range": "[7:0]",
                "clock": false,
                "size": 8
              },
              "position": {
                "x": -56,
                "y": 368
              }
            },
            {
              "id": "173fc234-1ee8-43a2-b1a1-67a40b6df2b9",
              "type": "d5cfed2163a066dde88aa02c01ea6737a486a660",
              "position": {
                "x": 176,
                "y": 104
              },
              "size": {
                "width": 96,
                "height": 128
              }
            },
            {
              "id": "8ab61228-4a49-4cfb-b97d-168790801faa",
              "type": "27fd6d7be6fb06056b62628fddb7d34bbc1f2eda",
              "position": {
                "x": 624,
                "y": 352
              },
              "size": {
                "width": 96,
                "height": 64
              }
            },
            {
              "id": "a2963f38-a6e5-4f77-9c72-39fa6264f6ad",
              "type": "26f57ab819923cfb72331d71a53aae8ab0714455",
              "position": {
                "x": 392,
                "y": 136
              },
              "size": {
                "width": 96,
                "height": 64
              }
            },
            {
              "id": "11d8d93a-cb1f-4e0c-bd0d-d365fbdb06a0",
              "type": "469ef80a3ae189f282982cbffcc8e9fcbc0572b9",
              "position": {
                "x": 760,
                "y": 352
              },
              "size": {
                "width": 96,
                "height": 64
              }
            },
            {
              "id": "4e9dc663-f7c1-49bf-855d-bfc784d5a85f",
              "type": "3806a27270873b075f02a478fbd29f2785a37550",
              "position": {
                "x": 592,
                "y": -160
              },
              "size": {
                "width": 96,
                "height": 64
              }
            },
            {
              "id": "36ec2c4e-c3fd-4ec1-8927-e39f6e6d0542",
              "type": "594b58d900cd52ad49c2a0bef169dfc980a4fb02",
              "position": {
                "x": 616,
                "y": -56
              },
              "size": {
                "width": 96,
                "height": 64
              }
            },
            {
              "id": "667a91ec-478f-4dfc-83db-3dd76cd037e2",
              "type": "c8094338ed4d8fb414ec1f5289d0e9331ef4271a",
              "position": {
                "x": 424,
                "y": -56
              },
              "size": {
                "width": 96,
                "height": 64
              }
            },
            {
              "id": "3d3ea5f0-2b7f-46c2-b26f-958c118dbb30",
              "type": "basic.info",
              "data": {
                "info": "Generador de onda triangular a partir de diente de sierra",
                "readonly": true
              },
              "position": {
                "x": 360,
                "y": -168
              },
              "size": {
                "width": 144,
                "height": 120
              }
            },
            {
              "id": "9d2cfa08-1c0e-4b23-939e-27b53457f510",
              "type": "6348d5f32a0e5fd95eb3dc6bd3bad6bfd5305ef9",
              "position": {
                "x": 792,
                "y": -40
              },
              "size": {
                "width": 96,
                "height": 64
              }
            },
            {
              "id": "bb8c4721-c90f-428a-a397-753f0055f779",
              "type": "21cfcc19a4ad14c5fb5e8cfebd018ec356fe7542",
              "position": {
                "x": 616,
                "y": 32
              },
              "size": {
                "width": 96,
                "height": 64
              }
            }
          ],
          "wires": [
            {
              "source": {
                "block": "173fc234-1ee8-43a2-b1a1-67a40b6df2b9",
                "port": "5b9f64ea-71d3-49a0-9461-26ea68b27e98"
              },
              "target": {
                "block": "a2963f38-a6e5-4f77-9c72-39fa6264f6ad",
                "port": "65464f98-4fb4-4295-ab73-153196ce1d0d"
              },
              "size": 16
            },
            {
              "source": {
                "block": "8ab61228-4a49-4cfb-b97d-168790801faa",
                "port": "0344dacc-8583-456b-b377-8cb4ab97cf94"
              },
              "target": {
                "block": "11d8d93a-cb1f-4e0c-bd0d-d365fbdb06a0",
                "port": "cf3b4c7c-042a-45f7-b958-990d7157f928"
              }
            },
            {
              "source": {
                "block": "118151e1-05c3-4216-b12d-2b21049f0502",
                "port": "out"
              },
              "target": {
                "block": "173fc234-1ee8-43a2-b1a1-67a40b6df2b9",
                "port": "b8dd1ea3-13d1-447f-8daf-3ddd99b384c0"
              }
            },
            {
              "source": {
                "block": "e864596d-e7af-40f2-bbc5-5513482463b8",
                "port": "out"
              },
              "target": {
                "block": "173fc234-1ee8-43a2-b1a1-67a40b6df2b9",
                "port": "cc543352-26dd-485d-86ca-7ac6108cfd28"
              },
              "vertices": [
                {
                  "x": 80,
                  "y": 176
                }
              ],
              "size": 8
            },
            {
              "source": {
                "block": "b1329812-529b-4fa5-b4c8-dfa1ac73dda2",
                "port": "out"
              },
              "target": {
                "block": "173fc234-1ee8-43a2-b1a1-67a40b6df2b9",
                "port": "0646ad90-af20-4994-8632-8c7b1425787f"
              },
              "vertices": [
                {
                  "x": 104,
                  "y": 232
                }
              ]
            },
            {
              "source": {
                "block": "f76081f1-b9fb-4a18-b664-1e9748375486",
                "port": "out"
              },
              "target": {
                "block": "173fc234-1ee8-43a2-b1a1-67a40b6df2b9",
                "port": "aba8e8f2-4d5d-4fd4-b989-d3c60c5530c8"
              },
              "vertices": [
                {
                  "x": 136,
                  "y": 280
                }
              ]
            },
            {
              "source": {
                "block": "11d8d93a-cb1f-4e0c-bd0d-d365fbdb06a0",
                "port": "dee92aca-a960-46a7-8284-55b1a8c2024c"
              },
              "target": {
                "block": "feecf4a9-c6c4-49c5-a065-eb4de2a73243",
                "port": "in"
              },
              "size": 8
            },
            {
              "source": {
                "block": "a2963f38-a6e5-4f77-9c72-39fa6264f6ad",
                "port": "ae5a4023-0e63-4bdb-8963-31d3e3f13a90"
              },
              "target": {
                "block": "ab2710ae-8ee8-48a6-9ad8-21e610fcfe7c",
                "port": "in"
              },
              "size": 8
            },
            {
              "source": {
                "block": "a2963f38-a6e5-4f77-9c72-39fa6264f6ad",
                "port": "ae5a4023-0e63-4bdb-8963-31d3e3f13a90"
              },
              "target": {
                "block": "8ab61228-4a49-4cfb-b97d-168790801faa",
                "port": "7c52a7e8-0c3f-4fd2-b6b2-09ab82552b67"
              },
              "size": 8
            },
            {
              "source": {
                "block": "3f35a684-3dbc-4164-a88a-fbd61408fd0d",
                "port": "out"
              },
              "target": {
                "block": "8ab61228-4a49-4cfb-b97d-168790801faa",
                "port": "5ba675b8-ce46-46de-a8e9-e67aa61b8c4c"
              },
              "size": 8
            },
            {
              "source": {
                "block": "4e9dc663-f7c1-49bf-855d-bfc784d5a85f",
                "port": "1353bae3-53fb-465f-9582-d2aab45ebb42"
              },
              "target": {
                "block": "36ec2c4e-c3fd-4ec1-8927-e39f6e6d0542",
                "port": "e1acfe87-9854-4d0d-a348-a1139d9f48c0"
              },
              "vertices": [
                {
                  "x": 640,
                  "y": -72
                }
              ],
              "size": 7
            },
            {
              "source": {
                "block": "667a91ec-478f-4dfc-83db-3dd76cd037e2",
                "port": "5c07bd65-d4e4-463c-affc-8bffa37c3b11"
              },
              "target": {
                "block": "36ec2c4e-c3fd-4ec1-8927-e39f6e6d0542",
                "port": "a9fd2323-0a8b-496a-bafa-115e85deae1b"
              },
              "size": 7
            },
            {
              "source": {
                "block": "667a91ec-478f-4dfc-83db-3dd76cd037e2",
                "port": "42009c44-67c6-496d-ad9f-798dc3d7acb1"
              },
              "target": {
                "block": "4e9dc663-f7c1-49bf-855d-bfc784d5a85f",
                "port": "cf3b4c7c-042a-45f7-b958-990d7157f928"
              },
              "vertices": [
                {
                  "x": 544,
                  "y": -96
                }
              ]
            },
            {
              "source": {
                "block": "a2963f38-a6e5-4f77-9c72-39fa6264f6ad",
                "port": "ae5a4023-0e63-4bdb-8963-31d3e3f13a90"
              },
              "target": {
                "block": "667a91ec-478f-4dfc-83db-3dd76cd037e2",
                "port": "1f5c81aa-ebb1-4cd7-87fd-b9092de9a34f"
              },
              "vertices": [
                {
                  "x": 448,
                  "y": 80
                }
              ],
              "size": 8
            },
            {
              "source": {
                "block": "36ec2c4e-c3fd-4ec1-8927-e39f6e6d0542",
                "port": "dd207bd3-797a-426f-8da3-ca216dc59901"
              },
              "target": {
                "block": "9d2cfa08-1c0e-4b23-939e-27b53457f510",
                "port": "df2c6d7d-8c09-4531-b373-a690fd59dc8f"
              },
              "size": 7
            },
            {
              "source": {
                "block": "9d2cfa08-1c0e-4b23-939e-27b53457f510",
                "port": "f5e719c9-96af-4c63-a8bb-6440a98ace76"
              },
              "target": {
                "block": "aaf763ee-d70e-483d-8e97-23a19f75cc92",
                "port": "in"
              },
              "size": 8
            },
            {
              "source": {
                "block": "bb8c4721-c90f-428a-a397-753f0055f779",
                "port": "3d584b0a-29eb-47af-8c43-c0822282ef05"
              },
              "target": {
                "block": "9d2cfa08-1c0e-4b23-939e-27b53457f510",
                "port": "532326e6-86f0-471f-9a94-1941ea335483"
              }
            }
          ]
        }
      }
    },
    "d5cfed2163a066dde88aa02c01ea6737a486a660": {
      "package": {
        "name": "fasor_16_8",
        "version": "1",
        "description": "Fasor de 16 bits con incremento de 8 bits",
        "author": "Yago Torroja",
        "image": ""
      },
      "design": {
        "graph": {
          "blocks": [
            {
              "id": "b8dd1ea3-13d1-447f-8daf-3ddd99b384c0",
              "type": "basic.input",
              "data": {
                "name": "",
                "clock": true
              },
              "position": {
                "x": 312,
                "y": -48
              }
            },
            {
              "id": "cc543352-26dd-485d-86ca-7ac6108cfd28",
              "type": "basic.input",
              "data": {
                "name": "din",
                "range": "[7:0]",
                "clock": false,
                "size": 8
              },
              "position": {
                "x": 304,
                "y": 160
              }
            },
            {
              "id": "5b9f64ea-71d3-49a0-9461-26ea68b27e98",
              "type": "basic.output",
              "data": {
                "name": "fasor",
                "range": "[15:0]",
                "size": 16
              },
              "position": {
                "x": 1280,
                "y": 232
              }
            },
            {
              "id": "0646ad90-af20-4994-8632-8c7b1425787f",
              "type": "basic.input",
              "data": {
                "name": "load",
                "clock": false
              },
              "position": {
                "x": 304,
                "y": 240
              }
            },
            {
              "id": "aba8e8f2-4d5d-4fd4-b989-d3c60c5530c8",
              "type": "basic.input",
              "data": {
                "name": "Count",
                "clock": false
              },
              "position": {
                "x": 304,
                "y": 328
              }
            },
            {
              "id": "1bfd8ab6-dd1b-4499-8e72-3d48eb15523f",
              "type": "a53a7115252dabce2ee673317f6fd107bdbaf06b",
              "position": {
                "x": 880,
                "y": 232
              },
              "size": {
                "width": 96,
                "height": 64
              }
            },
            {
              "id": "8c41fcaa-e8d5-4a1b-bf3a-a4c14f73c3c3",
              "type": "3bf99b8d77aa1b29ec1bf618b53175de87f1001d",
              "position": {
                "x": 496,
                "y": 40
              },
              "size": {
                "width": 96,
                "height": 64
              }
            },
            {
              "id": "506d2997-f775-47f3-bd06-02b5f3097c02",
              "type": "1e224fc5e502be5be513db972bcbfb3cb609ca19",
              "position": {
                "x": 704,
                "y": 56
              },
              "size": {
                "width": 96,
                "height": 64
              }
            },
            {
              "id": "8177bd99-a7e6-419c-b29b-43973fee5b40",
              "type": "9d7f8fae631414bde9d1d0339e959dee072214f0",
              "position": {
                "x": 496,
                "y": 144
              },
              "size": {
                "width": 96,
                "height": 96
              }
            },
            {
              "id": "8c22db51-a798-4018-8278-84151ee0f3fe",
              "type": "7a5f2a7a13501ece42d58582e8902f6b410afeb4",
              "position": {
                "x": 1096,
                "y": 216
              },
              "size": {
                "width": 96,
                "height": 96
              }
            }
          ],
          "wires": [
            {
              "source": {
                "block": "8c41fcaa-e8d5-4a1b-bf3a-a4c14f73c3c3",
                "port": "ad29e130-c17a-4544-9237-4c72551ccbeb"
              },
              "target": {
                "block": "506d2997-f775-47f3-bd06-02b5f3097c02",
                "port": "59c2a16b-3910-4170-afa3-7c59279bdb2b"
              },
              "size": 8
            },
            {
              "source": {
                "block": "506d2997-f775-47f3-bd06-02b5f3097c02",
                "port": "84292ebb-60eb-48e2-bdac-43cdd0d604af"
              },
              "target": {
                "block": "1bfd8ab6-dd1b-4499-8e72-3d48eb15523f",
                "port": "f35a5a72-f387-4e19-b107-8fa1a9c9b968"
              },
              "size": 16
            },
            {
              "source": {
                "block": "1bfd8ab6-dd1b-4499-8e72-3d48eb15523f",
                "port": "bf745bae-936e-4767-95ad-d294bb6af0ab"
              },
              "target": {
                "block": "8c22db51-a798-4018-8278-84151ee0f3fe",
                "port": "87780977-3a6d-4d2b-bfa7-ce827d5674a0"
              },
              "size": 16
            },
            {
              "source": {
                "block": "8c22db51-a798-4018-8278-84151ee0f3fe",
                "port": "67160641-0126-4330-ae5d-89ebe6bc4f28"
              },
              "target": {
                "block": "5b9f64ea-71d3-49a0-9461-26ea68b27e98",
                "port": "in"
              },
              "size": 16
            },
            {
              "source": {
                "block": "8c22db51-a798-4018-8278-84151ee0f3fe",
                "port": "67160641-0126-4330-ae5d-89ebe6bc4f28"
              },
              "target": {
                "block": "1bfd8ab6-dd1b-4499-8e72-3d48eb15523f",
                "port": "5bdb5afd-19a6-40c3-a538-2fcca508ad50"
              },
              "vertices": [
                {
                  "x": 832,
                  "y": 384
                }
              ],
              "size": 16
            },
            {
              "source": {
                "block": "8177bd99-a7e6-419c-b29b-43973fee5b40",
                "port": "226cfa16-cde9-4666-9e5c-19bf28b6a763"
              },
              "target": {
                "block": "506d2997-f775-47f3-bd06-02b5f3097c02",
                "port": "996b11fb-c56a-43a7-bf9d-e90568a91f9d"
              },
              "size": 8
            },
            {
              "source": {
                "block": "0646ad90-af20-4994-8632-8c7b1425787f",
                "port": "out"
              },
              "target": {
                "block": "8177bd99-a7e6-419c-b29b-43973fee5b40",
                "port": "065ea371-8398-43b3-8341-287c234a3acb"
              },
              "vertices": [
                {
                  "x": 448,
                  "y": 248
                }
              ]
            },
            {
              "source": {
                "block": "cc543352-26dd-485d-86ca-7ac6108cfd28",
                "port": "out"
              },
              "target": {
                "block": "8177bd99-a7e6-419c-b29b-43973fee5b40",
                "port": "208a3ad9-22a5-425a-922f-94dfcf575052"
              },
              "size": 8
            },
            {
              "source": {
                "block": "b8dd1ea3-13d1-447f-8daf-3ddd99b384c0",
                "port": "out"
              },
              "target": {
                "block": "8177bd99-a7e6-419c-b29b-43973fee5b40",
                "port": "096f61b6-6d5c-4907-9512-e65b25969458"
              },
              "vertices": [
                {
                  "x": 448,
                  "y": 40
                }
              ]
            },
            {
              "source": {
                "block": "b8dd1ea3-13d1-447f-8daf-3ddd99b384c0",
                "port": "out"
              },
              "target": {
                "block": "8c22db51-a798-4018-8278-84151ee0f3fe",
                "port": "096f61b6-6d5c-4907-9512-e65b25969458"
              },
              "vertices": [
                {
                  "x": 936,
                  "y": -16
                },
                {
                  "x": 1024,
                  "y": 112
                }
              ]
            },
            {
              "source": {
                "block": "aba8e8f2-4d5d-4fd4-b989-d3c60c5530c8",
                "port": "out"
              },
              "target": {
                "block": "8c22db51-a798-4018-8278-84151ee0f3fe",
                "port": "065ea371-8398-43b3-8341-287c234a3acb"
              },
              "vertices": [
                {
                  "x": 1024,
                  "y": 312
                }
              ]
            }
          ]
        }
      }
    },
    "a53a7115252dabce2ee673317f6fd107bdbaf06b": {
      "package": {
        "name": "sum-2op-16bits",
        "version": "0.1",
        "description": "Sumador de dos operandos de 16 bits",
        "author": "Juan González-Gómez",
        "image": "%3Csvg%20xmlns=%22http://www.w3.org/2000/svg%22%20width=%22208.285%22%20height=%22208.61%22%20viewBox=%220%200%20195.2669%20195.57218%22%3E%3Ctext%20style=%22line-height:125%25%22%20x=%22-33.052%22%20y=%22195.572%22%20font-weight=%22400%22%20font-size=%22311.941%22%20letter-spacing=%220%22%20word-spacing=%220%22%20font-family=%22sans-serif%22%20stroke-width=%224.441%22%3E%3Ctspan%20x=%22-33.052%22%20y=%22195.572%22%20style=%22-inkscape-font-specification:'sans-serif%20Bold'%22%20font-weight=%22700%22%20fill=%22#00f%22%3E+%3C/tspan%3E%3C/text%3E%3C/svg%3E"
      },
      "design": {
        "graph": {
          "blocks": [
            {
              "id": "f35a5a72-f387-4e19-b107-8fa1a9c9b968",
              "type": "basic.input",
              "data": {
                "name": "",
                "range": "[15:0]",
                "clock": false,
                "size": 16
              },
              "position": {
                "x": 208,
                "y": 136
              }
            },
            {
              "id": "bf745bae-936e-4767-95ad-d294bb6af0ab",
              "type": "basic.output",
              "data": {
                "name": "",
                "range": "[15:0]",
                "size": 16
              },
              "position": {
                "x": 672,
                "y": 192
              }
            },
            {
              "id": "5bdb5afd-19a6-40c3-a538-2fcca508ad50",
              "type": "basic.input",
              "data": {
                "name": "",
                "range": "[15:0]",
                "clock": false,
                "size": 16
              },
              "position": {
                "x": 208,
                "y": 208
              }
            },
            {
              "id": "a8d15f9d-bba5-432f-b698-17964638c83a",
              "type": "basic.code",
              "data": {
                "code": "assign s = a + b;",
                "params": [],
                "ports": {
                  "in": [
                    {
                      "name": "a",
                      "range": "[15:0]",
                      "size": 16
                    },
                    {
                      "name": "b",
                      "range": "[15:0]",
                      "size": 16
                    }
                  ],
                  "out": [
                    {
                      "name": "s",
                      "range": "[15:0]",
                      "size": 16
                    }
                  ]
                }
              },
              "position": {
                "x": 384,
                "y": 192
              },
              "size": {
                "width": 216,
                "height": 64
              }
            }
          ],
          "wires": [
            {
              "source": {
                "block": "f35a5a72-f387-4e19-b107-8fa1a9c9b968",
                "port": "out"
              },
              "target": {
                "block": "a8d15f9d-bba5-432f-b698-17964638c83a",
                "port": "a"
              },
              "size": 16
            },
            {
              "source": {
                "block": "a8d15f9d-bba5-432f-b698-17964638c83a",
                "port": "s"
              },
              "target": {
                "block": "bf745bae-936e-4767-95ad-d294bb6af0ab",
                "port": "in"
              },
              "size": 16
            },
            {
              "source": {
                "block": "5bdb5afd-19a6-40c3-a538-2fcca508ad50",
                "port": "out"
              },
              "target": {
                "block": "a8d15f9d-bba5-432f-b698-17964638c83a",
                "port": "b"
              },
              "size": 16
            }
          ]
        }
      }
    },
    "3bf99b8d77aa1b29ec1bf618b53175de87f1001d": {
      "package": {
        "name": "Valor_0_8bits",
        "version": "0.0.1",
        "description": "Valor constante 0 para bus de 8 bits",
        "author": "Juan Gonzalez-Gomez (Obijuan)",
        "image": "%3Csvg%20xmlns=%22http://www.w3.org/2000/svg%22%20width=%22346.308%22%20height=%22300.445%22%20viewBox=%220%200%20324.66403%20281.66758%22%3E%3Cg%20font-weight=%22400%22%20font-family=%22Ubuntu%20Mono%22%20letter-spacing=%220%22%20word-spacing=%220%22%3E%3Ctext%20style=%22line-height:125%25;-inkscape-font-specification:'Ubuntu%20Mono'%22%20x=%22371.115%22%20y=%22653.344%22%20font-size=%22335.399%22%20fill=%22green%22%20transform=%22translate(-326.544%20-441.037)%22%3E%3Ctspan%20x=%22371.115%22%20y=%22653.344%22%3E0%3C/tspan%3E%3C/text%3E%3Ctext%20style=%22line-height:125%25;-inkscape-font-specification:'Ubuntu%20Mono'%22%20x=%22322.722%22%20y=%22721.624%22%20font-size=%2283.077%22%20transform=%22translate(-326.544%20-441.037)%22%3E%3Ctspan%20x=%22322.722%22%20y=%22721.624%22%3E00000000%3C/tspan%3E%3C/text%3E%3Ctext%20style=%22line-height:125%25;-inkscape-font-specification:'Ubuntu%20Mono'%22%20x=%22548.722%22%20y=%22651.624%22%20font-size=%2283.077%22%20fill=%22#00f%22%20transform=%22translate(-326.544%20-441.037)%22%3E%3Ctspan%20x=%22548.722%22%20y=%22651.624%22%3E00%3C/tspan%3E%3C/text%3E%3C/g%3E%3C/svg%3E"
      },
      "design": {
        "graph": {
          "blocks": [
            {
              "id": "ad29e130-c17a-4544-9237-4c72551ccbeb",
              "type": "basic.output",
              "data": {
                "name": "k",
                "range": "[7:0]",
                "size": 8
              },
              "position": {
                "x": 928,
                "y": 256
              }
            },
            {
              "id": "ad7918eb-22dd-4b6c-949f-e428f5a55530",
              "type": "basic.constant",
              "data": {
                "name": "",
                "value": "0",
                "local": true
              },
              "position": {
                "x": 728,
                "y": 152
              }
            },
            {
              "id": "d8755f82-ee3e-47e9-9ac3-e4d6713a6105",
              "type": "5ad97e1e35a295d0ec722addd6df97c806fc6b7c",
              "position": {
                "x": 728,
                "y": 256
              },
              "size": {
                "width": 96,
                "height": 64
              }
            }
          ],
          "wires": [
            {
              "source": {
                "block": "ad7918eb-22dd-4b6c-949f-e428f5a55530",
                "port": "constant-out"
              },
              "target": {
                "block": "d8755f82-ee3e-47e9-9ac3-e4d6713a6105",
                "port": "c0fb4784-5e8c-4f41-9f4b-6daa2e9e03a4"
              }
            },
            {
              "source": {
                "block": "d8755f82-ee3e-47e9-9ac3-e4d6713a6105",
                "port": "a9d6830d-5cc7-4f63-a068-35181d2537bc"
              },
              "target": {
                "block": "ad29e130-c17a-4544-9237-4c72551ccbeb",
                "port": "in"
              },
              "size": 8
            }
          ]
        }
      }
    },
    "5ad97e1e35a295d0ec722addd6df97c806fc6b7c": {
      "package": {
        "name": "Constante-8bits",
        "version": "0.0.1",
        "description": "Valor genérico constante, de 8 bits. Su valor se introduce como parámetro. Por defecto vale 0",
        "author": "Juan Gonzalez-Gomez (Obijuan)",
        "image": "%3Csvg%20xmlns=%22http://www.w3.org/2000/svg%22%20width=%22145.608%22%20height=%22247.927%22%20viewBox=%220%200%20136.50729%20232.43134%22%3E%3Ctext%20style=%22line-height:125%25;-inkscape-font-specification:'Ubuntu%20Mono'%22%20x=%22293.115%22%20y=%22648.344%22%20font-weight=%22400%22%20font-size=%22335.399%22%20font-family=%22Ubuntu%20Mono%22%20letter-spacing=%220%22%20word-spacing=%220%22%20fill=%22green%22%20transform=%22translate(-316.929%20-415.913)%22%3E%3Ctspan%20x=%22293.115%22%20y=%22648.344%22%3Ek%3C/tspan%3E%3C/text%3E%3C/svg%3E"
      },
      "design": {
        "graph": {
          "blocks": [
            {
              "id": "a9d6830d-5cc7-4f63-a068-35181d2537bc",
              "type": "basic.output",
              "data": {
                "name": "k",
                "range": "[7:0]",
                "size": 8
              },
              "position": {
                "x": 960,
                "y": 248
              }
            },
            {
              "id": "c0fb4784-5e8c-4f41-9f4b-6daa2e9e03a4",
              "type": "basic.constant",
              "data": {
                "name": "",
                "value": "0",
                "local": false
              },
              "position": {
                "x": 728,
                "y": 128
              }
            },
            {
              "id": "7dbe7521-0f9f-43ee-ab0c-0439e2c20bc2",
              "type": "basic.code",
              "data": {
                "code": "assign k = VALUE;",
                "params": [
                  {
                    "name": "VALUE"
                  }
                ],
                "ports": {
                  "in": [],
                  "out": [
                    {
                      "name": "k",
                      "range": "[7:0]",
                      "size": 8
                    }
                  ]
                }
              },
              "position": {
                "x": 672,
                "y": 248
              },
              "size": {
                "width": 208,
                "height": 64
              }
            }
          ],
          "wires": [
            {
              "source": {
                "block": "c0fb4784-5e8c-4f41-9f4b-6daa2e9e03a4",
                "port": "constant-out"
              },
              "target": {
                "block": "7dbe7521-0f9f-43ee-ab0c-0439e2c20bc2",
                "port": "VALUE"
              }
            },
            {
              "source": {
                "block": "7dbe7521-0f9f-43ee-ab0c-0439e2c20bc2",
                "port": "k"
              },
              "target": {
                "block": "a9d6830d-5cc7-4f63-a068-35181d2537bc",
                "port": "in"
              },
              "size": 8
            }
          ]
        }
      }
    },
    "1e224fc5e502be5be513db972bcbfb3cb609ca19": {
      "package": {
        "name": "Agregador-bus",
        "version": "0.1",
        "description": "Agregador de 2 buses de 8-bits a bus de 16-bits",
        "author": "Juan González-Gómez (Obijuan)",
        "image": "%3Csvg%20xmlns=%22http://www.w3.org/2000/svg%22%20width=%22354.768%22%20height=%22241.058%22%20viewBox=%220%200%20332.59497%20225.99201%22%3E%3Cpath%20d=%22M164.218%2077.643L103.07%2016.705C92.386%206.25%2078.036.461%2063.11.5H.5v26.186l61.698.046c8.012-.043%2015.705%203.133%2021.47%208.81l61.448%2061.315a57.292%2057.292%200%200%200%2039.993%2016.139%2057.292%2057.292%200%200%200-39.993%2016.14L83.668%20190.45c-5.765%205.677-13.458%208.853-21.47%208.81L.5%20199.306v26.186h62.612c14.924.039%2029.463-5.9%2040.204-16.28l60.902-60.863a29.857%2029.857%200%200%201%2021.347-8.81l146.53-.113V86.457H185.571a29.884%2029.884%200%200%201-21.353-8.814z%22%20fill=%22green%22%20stroke=%22#000%22%20stroke-linecap=%22round%22%20stroke-linejoin=%22round%22/%3E%3C/svg%3E"
      },
      "design": {
        "graph": {
          "blocks": [
            {
              "id": "59c2a16b-3910-4170-afa3-7c59279bdb2b",
              "type": "basic.input",
              "data": {
                "name": "i1",
                "range": "[7:0]",
                "clock": false,
                "size": 8
              },
              "position": {
                "x": 112,
                "y": 144
              }
            },
            {
              "id": "84292ebb-60eb-48e2-bdac-43cdd0d604af",
              "type": "basic.output",
              "data": {
                "name": "o",
                "range": "[15:0]",
                "size": 16
              },
              "position": {
                "x": 584,
                "y": 200
              }
            },
            {
              "id": "996b11fb-c56a-43a7-bf9d-e90568a91f9d",
              "type": "basic.input",
              "data": {
                "name": "i0",
                "range": "[7:0]",
                "clock": false,
                "size": 8
              },
              "position": {
                "x": 112,
                "y": 272
              }
            },
            {
              "id": "16e78204-213e-4833-9096-89d735307ec2",
              "type": "basic.code",
              "data": {
                "code": "assign o = {i1, i0};\n",
                "params": [],
                "ports": {
                  "in": [
                    {
                      "name": "i1",
                      "range": "[7:0]",
                      "size": 8
                    },
                    {
                      "name": "i0",
                      "range": "[7:0]",
                      "size": 8
                    }
                  ],
                  "out": [
                    {
                      "name": "o",
                      "range": "[15:0]",
                      "size": 16
                    }
                  ]
                }
              },
              "position": {
                "x": 296,
                "y": 176
              },
              "size": {
                "width": 224,
                "height": 112
              }
            }
          ],
          "wires": [
            {
              "source": {
                "block": "996b11fb-c56a-43a7-bf9d-e90568a91f9d",
                "port": "out"
              },
              "target": {
                "block": "16e78204-213e-4833-9096-89d735307ec2",
                "port": "i0"
              },
              "size": 8
            },
            {
              "source": {
                "block": "59c2a16b-3910-4170-afa3-7c59279bdb2b",
                "port": "out"
              },
              "target": {
                "block": "16e78204-213e-4833-9096-89d735307ec2",
                "port": "i1"
              },
              "size": 8
            },
            {
              "source": {
                "block": "16e78204-213e-4833-9096-89d735307ec2",
                "port": "o"
              },
              "target": {
                "block": "84292ebb-60eb-48e2-bdac-43cdd0d604af",
                "port": "in"
              },
              "size": 16
            }
          ]
        }
      }
    },
    "7a5f2a7a13501ece42d58582e8902f6b410afeb4": {
      "package": {
        "name": "Reg_16_en",
        "version": "1",
        "description": "Registro paralelo de 16 bits con habilitación (enable)",
        "author": "Yago Torroja",
        "image": ""
      },
      "design": {
        "graph": {
          "blocks": [
            {
              "id": "096f61b6-6d5c-4907-9512-e65b25969458",
              "type": "basic.input",
              "data": {
                "name": "clk",
                "clock": true
              },
              "position": {
                "x": 80,
                "y": 176
              }
            },
            {
              "id": "87780977-3a6d-4d2b-bfa7-ce827d5674a0",
              "type": "basic.input",
              "data": {
                "name": "d",
                "range": "[15:0]",
                "clock": false,
                "size": 16
              },
              "position": {
                "x": 80,
                "y": 248
              }
            },
            {
              "id": "67160641-0126-4330-ae5d-89ebe6bc4f28",
              "type": "basic.output",
              "data": {
                "name": "q",
                "range": "[15:0]",
                "size": 16
              },
              "position": {
                "x": 632,
                "y": 248
              }
            },
            {
              "id": "065ea371-8398-43b3-8341-287c234a3acb",
              "type": "basic.input",
              "data": {
                "name": "enable",
                "clock": false
              },
              "position": {
                "x": 80,
                "y": 312
              }
            },
            {
              "id": "f3b434e4-0c8f-4dd7-90c7-305189a807f1",
              "type": "basic.constant",
              "data": {
                "name": "",
                "value": "0",
                "local": false
              },
              "position": {
                "x": 376,
                "y": 56
              }
            },
            {
              "id": "32106310-bfdc-41db-9a7c-2dadd5016c3f",
              "type": "basic.code",
              "data": {
                "code": "localparam N = 16;\n\nreg [N-1:0] q = INI;\n\nalways @(posedge clk)\n  if (en)\n    q <= d;",
                "params": [
                  {
                    "name": "INI"
                  }
                ],
                "ports": {
                  "in": [
                    {
                      "name": "clk"
                    },
                    {
                      "name": "d",
                      "range": "[15:0]",
                      "size": 16
                    },
                    {
                      "name": "en"
                    }
                  ],
                  "out": [
                    {
                      "name": "q",
                      "range": "[15:0]",
                      "size": 16
                    }
                  ]
                }
              },
              "position": {
                "x": 280,
                "y": 176
              },
              "size": {
                "width": 288,
                "height": 200
              }
            }
          ],
          "wires": [
            {
              "source": {
                "block": "f3b434e4-0c8f-4dd7-90c7-305189a807f1",
                "port": "constant-out"
              },
              "target": {
                "block": "32106310-bfdc-41db-9a7c-2dadd5016c3f",
                "port": "INI"
              },
              "vertices": []
            },
            {
              "source": {
                "block": "096f61b6-6d5c-4907-9512-e65b25969458",
                "port": "out"
              },
              "target": {
                "block": "32106310-bfdc-41db-9a7c-2dadd5016c3f",
                "port": "clk"
              }
            },
            {
              "source": {
                "block": "065ea371-8398-43b3-8341-287c234a3acb",
                "port": "out"
              },
              "target": {
                "block": "32106310-bfdc-41db-9a7c-2dadd5016c3f",
                "port": "en"
              }
            },
            {
              "source": {
                "block": "32106310-bfdc-41db-9a7c-2dadd5016c3f",
                "port": "q"
              },
              "target": {
                "block": "67160641-0126-4330-ae5d-89ebe6bc4f28",
                "port": "in"
              },
              "size": 16
            },
            {
              "source": {
                "block": "87780977-3a6d-4d2b-bfa7-ce827d5674a0",
                "port": "out"
              },
              "target": {
                "block": "32106310-bfdc-41db-9a7c-2dadd5016c3f",
                "port": "d"
              },
              "size": 16
            }
          ]
        }
      }
    },
    "27fd6d7be6fb06056b62628fddb7d34bbc1f2eda": {
      "package": {
        "name": "Menor-que-2-op",
        "version": "0.1",
        "description": "Comparador menor que, de dos operandos de 8 bits",
        "author": "Juan Gonzalez-Gomez (Obijuan)",
        "image": "%3Csvg%20xmlns=%22http://www.w3.org/2000/svg%22%20width=%22732.741%22%20height=%22283.481%22%20viewBox=%220%200%20193.87093%2075.00425%22%3E%3Cg%20word-spacing=%220%22%20letter-spacing=%220%22%20font-weight=%22400%22%20font-family=%22sans-serif%22%20stroke-width=%22.206%22%20fill=%22#00f%22%3E%3Ctext%20font-size=%2296.3%22%20y=%2259.926%22%20x=%2211.384%22%20style=%22line-height:125%25%22%20stroke-width=%22.057%22%20transform=%22matrix(1.37272%200%200%201.37272%2024.05%20-3.32)%22%3E%3Ctspan%20font-weight=%22700%22%20style=%22-inkscape-font-specification:'sans-serif%20Bold'%22%20y=%2259.926%22%20x=%2211.384%22%20stroke-width=%22.206%22%3E&lt;%3C/tspan%3E%3C/text%3E%3Ctext%20font-size=%2249.515%22%20y=%2241.865%22%20x=%22-19.647%22%20style=%22line-height:125%25%22%20stroke-width=%22.029%22%20transform=%22matrix(1.37272%200%200%201.37272%2024.05%20-3.32)%22%3E%3Ctspan%20font-weight=%22700%22%20style=%22-inkscape-font-specification:'sans-serif%20Bold'%22%20y=%2241.865%22%20x=%22-19.647%22%20stroke-width=%22.106%22%3Ea%3C/tspan%3E%3C/text%3E%3Ctext%20font-size=%2249.515%22%20y=%2241.865%22%20x=%2290.492%22%20style=%22line-height:125%25%22%20stroke-width=%22.029%22%20transform=%22matrix(1.37272%200%200%201.37272%2024.05%20-3.32)%22%3E%3Ctspan%20font-weight=%22700%22%20style=%22-inkscape-font-specification:'sans-serif%20Bold'%22%20y=%2241.865%22%20x=%2290.492%22%20stroke-width=%22.106%22%3Eb%3C/tspan%3E%3C/text%3E%3C/g%3E%3C/svg%3E"
      },
      "design": {
        "graph": {
          "blocks": [
            {
              "id": "7c52a7e8-0c3f-4fd2-b6b2-09ab82552b67",
              "type": "basic.input",
              "data": {
                "name": "a",
                "range": "[7:0]",
                "clock": false,
                "size": 8
              },
              "position": {
                "x": 152,
                "y": 104
              }
            },
            {
              "id": "0344dacc-8583-456b-b377-8cb4ab97cf94",
              "type": "basic.output",
              "data": {
                "name": ""
              },
              "position": {
                "x": 616,
                "y": 160
              }
            },
            {
              "id": "5ba675b8-ce46-46de-a8e9-e67aa61b8c4c",
              "type": "basic.input",
              "data": {
                "name": "b",
                "range": "[7:0]",
                "clock": false,
                "size": 8
              },
              "position": {
                "x": 152,
                "y": 200
              }
            },
            {
              "id": "9c811723-c900-4ceb-9989-036b071ee3fe",
              "type": "basic.code",
              "data": {
                "code": "assign eq = (a < b);",
                "params": [],
                "ports": {
                  "in": [
                    {
                      "name": "a",
                      "range": "[7:0]",
                      "size": 8
                    },
                    {
                      "name": "b",
                      "range": "[7:0]",
                      "size": 8
                    }
                  ],
                  "out": [
                    {
                      "name": "eq"
                    }
                  ]
                }
              },
              "position": {
                "x": 344,
                "y": 160
              },
              "size": {
                "width": 224,
                "height": 64
              }
            }
          ],
          "wires": [
            {
              "source": {
                "block": "9c811723-c900-4ceb-9989-036b071ee3fe",
                "port": "eq"
              },
              "target": {
                "block": "0344dacc-8583-456b-b377-8cb4ab97cf94",
                "port": "in"
              }
            },
            {
              "source": {
                "block": "5ba675b8-ce46-46de-a8e9-e67aa61b8c4c",
                "port": "out"
              },
              "target": {
                "block": "9c811723-c900-4ceb-9989-036b071ee3fe",
                "port": "b"
              },
              "size": 8
            },
            {
              "source": {
                "block": "7c52a7e8-0c3f-4fd2-b6b2-09ab82552b67",
                "port": "out"
              },
              "target": {
                "block": "9c811723-c900-4ceb-9989-036b071ee3fe",
                "port": "a"
              },
              "size": 8
            }
          ]
        }
      }
    },
    "26f57ab819923cfb72331d71a53aae8ab0714455": {
      "package": {
        "name": "Separador-bus",
        "version": "0.1",
        "description": "Separador de bus de 16-bits en buses de 8 bits",
        "author": "Juan González-Gómez (Obijuan)",
        "image": "%3Csvg%20xmlns=%22http://www.w3.org/2000/svg%22%20width=%22354.768%22%20height=%22241.058%22%20viewBox=%220%200%20332.59497%20225.99201%22%3E%3Cpath%20d=%22M168.377%2077.643l61.147-60.938C240.21%206.25%20254.56.461%20269.484.5h62.611v26.186l-61.698.046c-8.012-.043-15.705%203.133-21.47%208.81L187.48%2096.857a57.292%2057.292%200%200%201-39.993%2016.139%2057.292%2057.292%200%200%201%2039.993%2016.14l61.448%2061.314c5.765%205.677%2013.458%208.853%2021.47%208.81l61.698.046v26.186h-62.612c-14.924.039-29.463-5.9-40.204-16.28l-60.902-60.863a29.857%2029.857%200%200%200-21.347-8.81L.5%20139.427V86.457h146.524a29.884%2029.884%200%200%200%2021.353-8.814z%22%20fill=%22green%22%20stroke=%22#000%22%20stroke-linecap=%22round%22%20stroke-linejoin=%22round%22/%3E%3C/svg%3E"
      },
      "design": {
        "graph": {
          "blocks": [
            {
              "id": "ae5a4023-0e63-4bdb-8963-31d3e3f13a90",
              "type": "basic.output",
              "data": {
                "name": "o1",
                "range": "[7:0]",
                "size": 8
              },
              "position": {
                "x": 584,
                "y": 144
              }
            },
            {
              "id": "65464f98-4fb4-4295-ab73-153196ce1d0d",
              "type": "basic.input",
              "data": {
                "name": "i",
                "range": "[15:0]",
                "clock": false,
                "size": 16
              },
              "position": {
                "x": 120,
                "y": 200
              }
            },
            {
              "id": "c675bded-7009-4371-b928-dadba3620245",
              "type": "basic.output",
              "data": {
                "name": "o0",
                "range": "[7:0]",
                "size": 8
              },
              "position": {
                "x": 584,
                "y": 272
              }
            },
            {
              "id": "16e78204-213e-4833-9096-89d735307ec2",
              "type": "basic.code",
              "data": {
                "code": "assign o1 = i[15:8];\nassign o0 = i[7:0];",
                "params": [],
                "ports": {
                  "in": [
                    {
                      "name": "i",
                      "range": "[15:0]",
                      "size": 16
                    }
                  ],
                  "out": [
                    {
                      "name": "o1",
                      "range": "[7:0]",
                      "size": 8
                    },
                    {
                      "name": "o0",
                      "range": "[7:0]",
                      "size": 8
                    }
                  ]
                }
              },
              "position": {
                "x": 296,
                "y": 176
              },
              "size": {
                "width": 208,
                "height": 112
              }
            }
          ],
          "wires": [
            {
              "source": {
                "block": "16e78204-213e-4833-9096-89d735307ec2",
                "port": "o0"
              },
              "target": {
                "block": "c675bded-7009-4371-b928-dadba3620245",
                "port": "in"
              },
              "size": 8
            },
            {
              "source": {
                "block": "16e78204-213e-4833-9096-89d735307ec2",
                "port": "o1"
              },
              "target": {
                "block": "ae5a4023-0e63-4bdb-8963-31d3e3f13a90",
                "port": "in"
              },
              "size": 8
            },
            {
              "source": {
                "block": "65464f98-4fb4-4295-ab73-153196ce1d0d",
                "port": "out"
              },
              "target": {
                "block": "16e78204-213e-4833-9096-89d735307ec2",
                "port": "i"
              },
              "size": 16
            }
          ]
        }
      }
    },
    "469ef80a3ae189f282982cbffcc8e9fcbc0572b9": {
      "package": {
        "name": "mult-1-8-bus",
        "version": "0.1",
        "description": "Multiplicador de cables. Genera un bus de 8 bits, a patir del bit de entrada",
        "author": "",
        "image": "%3Csvg%20xmlns=%22http://www.w3.org/2000/svg%22%20width=%22352.5%22%20height=%22132.123%22%20viewBox=%220%200%2093.265732%2034.957444%22%3E%3Cg%20transform=%22translate(-44.148%20-114.575)%22%3E%3Crect%20width=%2292.737%22%20height=%227.314%22%20x=%22-137.149%22%20y=%22141.954%22%20ry=%220%22%20transform=%22scale(-1%201)%22%20fill=%22green%22%20stroke=%22#000%22%20stroke-width=%22.529%22%20stroke-linecap=%22round%22%20stroke-linejoin=%22bevel%22/%3E%3Ctext%20style=%22line-height:1.25;-inkscape-font-specification:ubuntu;text-align:start%22%20x=%22108.948%22%20y=%22135.274%22%20font-weight=%22400%22%20font-size=%2229.868%22%20font-family=%22ubuntu%22%20letter-spacing=%220%22%20word-spacing=%220%22%20stroke-width=%22.265%22%3E%3Ctspan%20x=%22108.948%22%20y=%22135.274%22%3EX%3C/tspan%3E%3C/text%3E%3C/g%3E%3C/svg%3E"
      },
      "design": {
        "graph": {
          "blocks": [
            {
              "id": "cf3b4c7c-042a-45f7-b958-990d7157f928",
              "type": "basic.input",
              "data": {
                "name": "",
                "clock": false
              },
              "position": {
                "x": 464,
                "y": 144
              }
            },
            {
              "id": "dee92aca-a960-46a7-8284-55b1a8c2024c",
              "type": "basic.output",
              "data": {
                "name": "",
                "range": "[7:0]",
                "size": 8
              },
              "position": {
                "x": 960,
                "y": 144
              }
            },
            {
              "id": "3266f4f1-eba1-4272-a937-4415542dcb7f",
              "type": "basic.code",
              "data": {
                "code": "assign o = {8{i}};\n",
                "params": [],
                "ports": {
                  "in": [
                    {
                      "name": "i"
                    }
                  ],
                  "out": [
                    {
                      "name": "o",
                      "range": "[7:0]",
                      "size": 8
                    }
                  ]
                }
              },
              "position": {
                "x": 632,
                "y": 144
              },
              "size": {
                "width": 224,
                "height": 64
              }
            }
          ],
          "wires": [
            {
              "source": {
                "block": "cf3b4c7c-042a-45f7-b958-990d7157f928",
                "port": "out"
              },
              "target": {
                "block": "3266f4f1-eba1-4272-a937-4415542dcb7f",
                "port": "i"
              }
            },
            {
              "source": {
                "block": "3266f4f1-eba1-4272-a937-4415542dcb7f",
                "port": "o"
              },
              "target": {
                "block": "dee92aca-a960-46a7-8284-55b1a8c2024c",
                "port": "in"
              },
              "size": 8
            }
          ]
        }
      }
    },
    "3806a27270873b075f02a478fbd29f2785a37550": {
      "package": {
        "name": "mult-1-7-bus",
        "version": "0.1",
        "description": "Multiplicador de cables. Genera un bus de 7 bits, a patir del bit de entrada",
        "author": "",
        "image": "%3Csvg%20xmlns=%22http://www.w3.org/2000/svg%22%20width=%22352.5%22%20height=%22132.123%22%20viewBox=%220%200%2093.265732%2034.957444%22%3E%3Cg%20transform=%22translate(-44.148%20-114.575)%22%3E%3Crect%20width=%2292.737%22%20height=%227.314%22%20x=%22-137.149%22%20y=%22141.954%22%20ry=%220%22%20transform=%22scale(-1%201)%22%20fill=%22green%22%20stroke=%22#000%22%20stroke-width=%22.529%22%20stroke-linecap=%22round%22%20stroke-linejoin=%22bevel%22/%3E%3Ctext%20style=%22line-height:1.25;-inkscape-font-specification:ubuntu;text-align:start%22%20x=%22108.948%22%20y=%22135.274%22%20font-weight=%22400%22%20font-size=%2229.868%22%20font-family=%22ubuntu%22%20letter-spacing=%220%22%20word-spacing=%220%22%20stroke-width=%22.265%22%3E%3Ctspan%20x=%22108.948%22%20y=%22135.274%22%3EX%3C/tspan%3E%3C/text%3E%3C/g%3E%3C/svg%3E"
      },
      "design": {
        "graph": {
          "blocks": [
            {
              "id": "cf3b4c7c-042a-45f7-b958-990d7157f928",
              "type": "basic.input",
              "data": {
                "name": "",
                "clock": false
              },
              "position": {
                "x": 464,
                "y": 144
              }
            },
            {
              "id": "1353bae3-53fb-465f-9582-d2aab45ebb42",
              "type": "basic.output",
              "data": {
                "name": "",
                "range": "[6:0]",
                "size": 7
              },
              "position": {
                "x": 960,
                "y": 144
              }
            },
            {
              "id": "3266f4f1-eba1-4272-a937-4415542dcb7f",
              "type": "basic.code",
              "data": {
                "code": "assign o = {7{i}};\n",
                "params": [],
                "ports": {
                  "in": [
                    {
                      "name": "i"
                    }
                  ],
                  "out": [
                    {
                      "name": "o",
                      "range": "[6:0]",
                      "size": 7
                    }
                  ]
                }
              },
              "position": {
                "x": 632,
                "y": 144
              },
              "size": {
                "width": 224,
                "height": 64
              }
            }
          ],
          "wires": [
            {
              "source": {
                "block": "cf3b4c7c-042a-45f7-b958-990d7157f928",
                "port": "out"
              },
              "target": {
                "block": "3266f4f1-eba1-4272-a937-4415542dcb7f",
                "port": "i"
              }
            },
            {
              "source": {
                "block": "3266f4f1-eba1-4272-a937-4415542dcb7f",
                "port": "o"
              },
              "target": {
                "block": "1353bae3-53fb-465f-9582-d2aab45ebb42",
                "port": "in"
              },
              "size": 7
            }
          ]
        }
      }
    },
    "594b58d900cd52ad49c2a0bef169dfc980a4fb02": {
      "package": {
        "name": "XOR-8",
        "version": "0.1",
        "description": "XOR bit a bit entre dos buses de 8 bits",
        "author": "Juan González-Gómez (Obijuan)",
        "image": "%3Csvg%20xmlns=%22http://www.w3.org/2000/svg%22%20height=%22193.047%22%20width=%22253.607%22%20version=%221%22%3E%3Cpath%20d=%22M126.011%20189.047H34.98s30.344-42.538%2031.085-94.03c.743-51.49-31.821-90.294-31.821-90.294l92.317-.394c46.445%201.948%20103.899%2053.44%20123.047%2093.678-32.601%2067.503-92.158%2089.79-123.596%2091.04z%22%20fill=%22none%22%20stroke=%22#000%22%20stroke-width=%228%22%20stroke-linecap=%22round%22%20stroke-linejoin=%22round%22/%3E%3Ctext%20x=%22129.011%22%20y=%22115.285%22%20font-size=%2258.24%22%20font-weight=%22400%22%20style=%22line-height:0%25%22%20font-family=%22sans-serif%22%20letter-spacing=%220%22%20word-spacing=%220%22%20fill=%22#00f%22%20transform=%22translate(-49.549%20.329)%22%3E%3Ctspan%20x=%22129.011%22%20y=%22115.285%22%20font-weight=%22700%22%3EXOR%3C/tspan%3E%3C/text%3E%3Cpath%20d=%22M4.772%20188.368s30.345-42.538%2031.086-94.03C36.6%2042.848%204.037%204.044%204.037%204.044%22%20fill=%22none%22%20stroke=%22#000%22%20stroke-width=%228%22%20stroke-linecap=%22round%22%20stroke-linejoin=%22round%22/%3E%3C/svg%3E"
      },
      "design": {
        "graph": {
          "blocks": [
            {
              "id": "e1acfe87-9854-4d0d-a348-a1139d9f48c0",
              "type": "basic.input",
              "data": {
                "name": "",
                "range": "[6:0]",
                "clock": false,
                "size": 7
              },
              "position": {
                "x": 112,
                "y": 120
              }
            },
            {
              "id": "dd207bd3-797a-426f-8da3-ca216dc59901",
              "type": "basic.output",
              "data": {
                "name": "",
                "range": "[6:0]",
                "size": 7
              },
              "position": {
                "x": 808,
                "y": 152
              }
            },
            {
              "id": "a9fd2323-0a8b-496a-bafa-115e85deae1b",
              "type": "basic.input",
              "data": {
                "name": "",
                "range": "[6:0]",
                "clock": false,
                "size": 7
              },
              "position": {
                "x": 112,
                "y": 184
              }
            },
            {
              "id": "b6b845d5-ee12-489e-ab57-02389c3f0e43",
              "type": "basic.code",
              "data": {
                "code": "assign o = a ^ b;\n",
                "params": [],
                "ports": {
                  "in": [
                    {
                      "name": "a",
                      "range": "[6:0]",
                      "size": 7
                    },
                    {
                      "name": "b",
                      "range": "[6:0]",
                      "size": 7
                    }
                  ],
                  "out": [
                    {
                      "name": "o",
                      "range": "[6:0]",
                      "size": 7
                    }
                  ]
                }
              },
              "position": {
                "x": 256,
                "y": 120
              },
              "size": {
                "width": 344,
                "height": 128
              }
            }
          ],
          "wires": [
            {
              "source": {
                "block": "b6b845d5-ee12-489e-ab57-02389c3f0e43",
                "port": "o"
              },
              "target": {
                "block": "dd207bd3-797a-426f-8da3-ca216dc59901",
                "port": "in"
              },
              "size": 7
            },
            {
              "source": {
                "block": "e1acfe87-9854-4d0d-a348-a1139d9f48c0",
                "port": "out"
              },
              "target": {
                "block": "b6b845d5-ee12-489e-ab57-02389c3f0e43",
                "port": "a"
              },
              "size": 7
            },
            {
              "source": {
                "block": "a9fd2323-0a8b-496a-bafa-115e85deae1b",
                "port": "out"
              },
              "target": {
                "block": "b6b845d5-ee12-489e-ab57-02389c3f0e43",
                "port": "b"
              },
              "size": 7
            }
          ]
        }
      }
    },
    "6348d5f32a0e5fd95eb3dc6bd3bad6bfd5305ef9": {
      "package": {
        "name": "Agregador-bus",
        "version": "0.1",
        "description": "Agregador de un bus de 7 bits y un cable a bus de 8-bits",
        "author": "Juan González-Gómez (Obijuan)",
        "image": "%3Csvg%20xmlns=%22http://www.w3.org/2000/svg%22%20width=%22354.768%22%20height=%22241.058%22%20viewBox=%220%200%20332.59497%20225.99201%22%3E%3Cpath%20d=%22M164.218%2077.643L103.07%2016.705C92.386%206.25%2078.036.461%2063.11.5H.5v26.186l61.698.046c8.012-.043%2015.705%203.133%2021.47%208.81l61.448%2061.315a57.292%2057.292%200%200%200%2039.993%2016.139%2057.292%2057.292%200%200%200-39.993%2016.14L83.668%20190.45c-5.765%205.677-13.458%208.853-21.47%208.81L.5%20199.306v26.186h62.612c14.924.039%2029.463-5.9%2040.204-16.28l60.902-60.863a29.857%2029.857%200%200%201%2021.347-8.81l146.53-.113V86.457H185.571a29.884%2029.884%200%200%201-21.353-8.814z%22%20fill=%22green%22%20stroke=%22#000%22%20stroke-linecap=%22round%22%20stroke-linejoin=%22round%22/%3E%3C/svg%3E"
      },
      "design": {
        "graph": {
          "blocks": [
            {
              "id": "df2c6d7d-8c09-4531-b373-a690fd59dc8f",
              "type": "basic.input",
              "data": {
                "name": "i1",
                "range": "[6:0]",
                "clock": false,
                "size": 7
              },
              "position": {
                "x": 112,
                "y": 144
              }
            },
            {
              "id": "f5e719c9-96af-4c63-a8bb-6440a98ace76",
              "type": "basic.output",
              "data": {
                "name": "o",
                "range": "[7:0]",
                "size": 8
              },
              "position": {
                "x": 584,
                "y": 200
              }
            },
            {
              "id": "532326e6-86f0-471f-9a94-1941ea335483",
              "type": "basic.input",
              "data": {
                "name": "i0",
                "clock": false
              },
              "position": {
                "x": 112,
                "y": 232
              }
            },
            {
              "id": "16e78204-213e-4833-9096-89d735307ec2",
              "type": "basic.code",
              "data": {
                "code": "assign o = {i1, i0};\n",
                "params": [],
                "ports": {
                  "in": [
                    {
                      "name": "i1",
                      "range": "[6:0]",
                      "size": 7
                    },
                    {
                      "name": "i0"
                    }
                  ],
                  "out": [
                    {
                      "name": "o",
                      "range": "[7:0]",
                      "size": 8
                    }
                  ]
                }
              },
              "position": {
                "x": 296,
                "y": 176
              },
              "size": {
                "width": 224,
                "height": 112
              }
            }
          ],
          "wires": [
            {
              "source": {
                "block": "df2c6d7d-8c09-4531-b373-a690fd59dc8f",
                "port": "out"
              },
              "target": {
                "block": "16e78204-213e-4833-9096-89d735307ec2",
                "port": "i1"
              },
              "size": 7
            },
            {
              "source": {
                "block": "532326e6-86f0-471f-9a94-1941ea335483",
                "port": "out"
              },
              "target": {
                "block": "16e78204-213e-4833-9096-89d735307ec2",
                "port": "i0"
              }
            },
            {
              "source": {
                "block": "16e78204-213e-4833-9096-89d735307ec2",
                "port": "o"
              },
              "target": {
                "block": "f5e719c9-96af-4c63-a8bb-6440a98ace76",
                "port": "in"
              },
              "size": 8
            }
          ]
        }
      }
    },
    "21cfcc19a4ad14c5fb5e8cfebd018ec356fe7542": {
      "package": {
        "name": "0",
        "version": "0.1",
        "description": "Un bit constante a 0",
        "author": "Jesus Arroyo",
        "image": "%3Csvg%20xmlns=%22http://www.w3.org/2000/svg%22%20width=%2233.563%22%20height=%2257.469%22%20viewBox=%220%200%2031.465601%2053.876499%22%3E%3Cpath%20d=%22M21.822%2032.843l4.092%208.992-3.772%209.727%204.181%201.31m-12.967-19.26s-1.091%208.253-2.585%208.919C9.278%2043.198%201%2049.389%201%2049.389l2.647%203.256%22%20fill=%22none%22%20stroke=%22green%22%20stroke-width=%222%22%20stroke-linecap=%22round%22%20stroke-linejoin=%22round%22/%3E%3Ctext%20style=%22line-height:125%25%22%20x=%22-.863%22%20y=%2230.575%22%20transform=%22scale(.90756%201.10186)%22%20font-weight=%22400%22%20font-size=%2254.594%22%20font-family=%22sans-serif%22%20letter-spacing=%220%22%20word-spacing=%220%22%20fill=%22green%22%3E%3Ctspan%20x=%22-.863%22%20y=%2230.575%22%20style=%22-inkscape-font-specification:'sans-serif%20Bold%20Italic'%22%20font-style=%22italic%22%20font-weight=%22700%22%3Eo%3C/tspan%3E%3C/text%3E%3C/svg%3E"
      },
      "design": {
        "graph": {
          "blocks": [
            {
              "id": "3d584b0a-29eb-47af-8c43-c0822282ef05",
              "type": "basic.output",
              "data": {
                "name": ""
              },
              "position": {
                "x": 512,
                "y": 160
              }
            },
            {
              "id": "61331ec5-2c56-4cdd-b607-e63b1502fa65",
              "type": "basic.code",
              "data": {
                "code": "//-- Bit constante a 0\nassign q = 1'b0;\n\n",
                "params": [],
                "ports": {
                  "in": [],
                  "out": [
                    {
                      "name": "q"
                    }
                  ]
                }
              },
              "position": {
                "x": 168,
                "y": 112
              },
              "size": {
                "width": 256,
                "height": 160
              }
            }
          ],
          "wires": [
            {
              "source": {
                "block": "61331ec5-2c56-4cdd-b607-e63b1502fa65",
                "port": "q"
              },
              "target": {
                "block": "3d584b0a-29eb-47af-8c43-c0822282ef05",
                "port": "in"
              }
            }
          ]
        }
      }
    },
    "5cfe2b453dc3bb6f0c6cd36ec4a333e08cb8902a": {
      "package": {
        "name": "DAC SD",
        "version": "1",
        "description": "DAC de 1 bit Sigma-Delta",
        "author": "Yago Torroja",
        "image": ""
      },
      "design": {
        "graph": {
          "blocks": [
            {
              "id": "4e96581f-dd60-43cf-9d48-d54baed25342",
              "type": "basic.input",
              "data": {
                "name": "",
                "clock": true
              },
              "position": {
                "x": -128,
                "y": 72
              }
            },
            {
              "id": "39638114-3d73-45fd-a5e0-da0f69729181",
              "type": "basic.input",
              "data": {
                "name": "in",
                "range": "[7:0]",
                "clock": false,
                "size": 8
              },
              "position": {
                "x": -128,
                "y": 192
              }
            },
            {
              "id": "69c4aebe-b5df-4d96-b48f-ce5732e0c48e",
              "type": "basic.output",
              "data": {
                "name": "out"
              },
              "position": {
                "x": 952,
                "y": 192
              }
            },
            {
              "id": "fd4dee99-964f-40ea-be44-605b57ea75dd",
              "type": "basic.input",
              "data": {
                "name": "en",
                "clock": false
              },
              "position": {
                "x": -128,
                "y": 304
              }
            },
            {
              "id": "c6356af9-dbd8-4b38-9aa4-43b4368b6050",
              "type": "basic.code",
              "data": {
                "code": "    localparam N = 8;\r\n    \r\n    reg [N-1+2:0] sigma;\r\n\r\n    wire [N-1+2:0] in_s;\r\n    wire [N-1+2:0] out_dac;\r\n    wire out_s;\r\n\r\n    // Generate output signal\r\n    assign out = out_s;\r\n\r\n    // input_s is in[] converted to unsigned+offset and extended with 2 bits\r\n    assign in_s    = {2'b00 , in[N-1], in[N-2:0]};\r\n    assign out_s   = sigma[N-1+2];\r\n    assign out_dac = {out_s, out_s, {N{1'b0}}};\r\n\r\n    always @(posedge clk)\r\n        if (en)\r\n            sigma <= sigma + in_s + out_dac;\r\n",
                "params": [],
                "ports": {
                  "in": [
                    {
                      "name": "clk"
                    },
                    {
                      "name": "in",
                      "range": "[7:0]",
                      "size": 8
                    },
                    {
                      "name": "en"
                    }
                  ],
                  "out": [
                    {
                      "name": "out"
                    }
                  ]
                }
              },
              "position": {
                "x": 136,
                "y": 48
              },
              "size": {
                "width": 712,
                "height": 344
              }
            }
          ],
          "wires": [
            {
              "source": {
                "block": "4e96581f-dd60-43cf-9d48-d54baed25342",
                "port": "out"
              },
              "target": {
                "block": "c6356af9-dbd8-4b38-9aa4-43b4368b6050",
                "port": "clk"
              }
            },
            {
              "source": {
                "block": "39638114-3d73-45fd-a5e0-da0f69729181",
                "port": "out"
              },
              "target": {
                "block": "c6356af9-dbd8-4b38-9aa4-43b4368b6050",
                "port": "in"
              },
              "size": 8
            },
            {
              "source": {
                "block": "fd4dee99-964f-40ea-be44-605b57ea75dd",
                "port": "out"
              },
              "target": {
                "block": "c6356af9-dbd8-4b38-9aa4-43b4368b6050",
                "port": "en"
              }
            },
            {
              "source": {
                "block": "c6356af9-dbd8-4b38-9aa4-43b4368b6050",
                "port": "out"
              },
              "target": {
                "block": "69c4aebe-b5df-4d96-b48f-ce5732e0c48e",
                "port": "in"
              }
            }
          ]
        }
      }
    }
  }
}