{
  "version": "1.2",
  "package": {
    "name": "Reg_16_en",
    "version": "1",
    "description": "Registro paralelo de 16 bits con habilitación (enable)",
    "author": "Yago Torroja",
    "image": ""
  },
  "design": {
    "board": "alhambra-ii",
    "graph": {
      "blocks": [
        {
          "id": "096f61b6-6d5c-4907-9512-e65b25969458",
          "type": "basic.input",
          "data": {
            "name": "clk",
            "clock": true,
            "virtual": true
          },
          "position": {
            "x": 80,
            "y": 176
          }
        },
        {
          "id": "87780977-3a6d-4d2b-bfa7-ce827d5674a0",
          "type": "basic.input",
          "data": {
            "name": "d",
            "range": "[15:0]",
            "clock": false,
            "size": 16,
            "virtual": true
          },
          "position": {
            "x": 80,
            "y": 248
          }
        },
        {
          "id": "67160641-0126-4330-ae5d-89ebe6bc4f28",
          "type": "basic.output",
          "data": {
            "name": "q",
            "range": "[15:0]",
            "size": 16,
            "virtual": true
          },
          "position": {
            "x": 632,
            "y": 248
          }
        },
        {
          "id": "065ea371-8398-43b3-8341-287c234a3acb",
          "type": "basic.input",
          "data": {
            "name": "enable",
            "clock": false,
            "virtual": true
          },
          "position": {
            "x": 80,
            "y": 312
          }
        },
        {
          "id": "f3b434e4-0c8f-4dd7-90c7-305189a807f1",
          "type": "basic.constant",
          "data": {
            "name": "",
            "value": "0",
            "local": false
          },
          "position": {
            "x": 376,
            "y": 56
          }
        },
        {
          "id": "32106310-bfdc-41db-9a7c-2dadd5016c3f",
          "type": "basic.code",
          "data": {
            "code": "localparam N = 16;\n\nreg [N-1:0] q = INI;\n\nalways @(posedge clk)\n  if (en)\n    q <= d;",
            "params": [
              {
                "name": "INI"
              }
            ],
            "ports": {
              "in": [
                {
                  "name": "clk"
                },
                {
                  "name": "d",
                  "range": "[15:0]",
                  "size": 16
                },
                {
                  "name": "en"
                }
              ],
              "out": [
                {
                  "name": "q",
                  "range": "[15:0]",
                  "size": 16
                }
              ]
            }
          },
          "position": {
            "x": 280,
            "y": 176
          },
          "size": {
            "width": 288,
            "height": 200
          }
        }
      ],
      "wires": [
        {
          "source": {
            "block": "f3b434e4-0c8f-4dd7-90c7-305189a807f1",
            "port": "constant-out"
          },
          "target": {
            "block": "32106310-bfdc-41db-9a7c-2dadd5016c3f",
            "port": "INI"
          },
          "vertices": []
        },
        {
          "source": {
            "block": "096f61b6-6d5c-4907-9512-e65b25969458",
            "port": "out"
          },
          "target": {
            "block": "32106310-bfdc-41db-9a7c-2dadd5016c3f",
            "port": "clk"
          }
        },
        {
          "source": {
            "block": "065ea371-8398-43b3-8341-287c234a3acb",
            "port": "out"
          },
          "target": {
            "block": "32106310-bfdc-41db-9a7c-2dadd5016c3f",
            "port": "en"
          }
        },
        {
          "source": {
            "block": "32106310-bfdc-41db-9a7c-2dadd5016c3f",
            "port": "q"
          },
          "target": {
            "block": "67160641-0126-4330-ae5d-89ebe6bc4f28",
            "port": "in"
          },
          "size": 16
        },
        {
          "source": {
            "block": "87780977-3a6d-4d2b-bfa7-ce827d5674a0",
            "port": "out"
          },
          "target": {
            "block": "32106310-bfdc-41db-9a7c-2dadd5016c3f",
            "port": "d"
          },
          "size": 16
        }
      ]
    }
  },
  "dependencies": {}
}